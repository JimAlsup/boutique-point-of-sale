
Boutique Point of Sale Directory Structure:
    -Serves a point of sale for a boutique (or other such event).
    -Excel files are used to as the actual database making it easy
     examine and manipulate the data.
    -Allows for the tracking of customer and customer sale information.
    -Individual sake item names are not tracked, but the artisan information
     on each item can be tracked.
    -Sale items can be track as taxable or not.
    -Cash, check, and Credit Cards can be tracked, but an external
     transaction system must be used for Credit cards.
    -Refund transactions can also be made.
    -Email and printed receipts are supported.
    -End of event reports can be generated for each artisan.

Windows Installer: (Inno based)
    -Output/setup.exe is a single executable installer
    -Installs to program files(x86)\BoutiquePOS\
    -pos.exe is the executable name and is packaged as an executable
    -The font Adine KirnBerg.ttf (free for all uses) is installed to the
     system font directory
    -The data directory in the program directory is setup as writable and
     contains a file master_dir.txt. This files only purpose is to store
     the configured database location.

Database directory file structure:
    -Multiple directories can be used on single computer to track multiple
     types of boutiques, but this is not an expected use case as the client
     information would be separate.
    -If the database directory is shared out more sales terminals can be setup
     by simply browsing to the shared directory.
    -artists.xlsx        Iinitial copy from artists_template.xlsx in program director
    -clients.xlsx        Initial copy from clientes_template.xlsx in progrm directory
    -misc_data.json      Various setup/admin items stored in json format.  Auto created
                         when needed.
    -transaction_no.txt  Created on first need contains the last transaction id and serves
                         as a semaphore (file lock) when multiple point of sales are used
    <event dir names>    Created when defining a new event and contains all event
                         related data.  Events list are take from the actual list of directories
                         and not stored elsewhere.

Event directory file structure:
    -event_artists.xlsx   Event specific data for each artisan in artist.xlsx
    -receipt.log          Log of all receipts generated and emailed to clients
    -sale_records.xlsx    Excel records of ever event sale/refund


Python packages that must be installed with Python for development
    -openpyxl         excel file access
    -portalocker      for file locking which enables multiple sale terminals
    -cx_freeze        for packaging the various python source files and python itself into single
                      zippable self contained directory
    -inno setup       Windows setup program used to create a single setup executable based on the
                      cx_freeze generated directory
