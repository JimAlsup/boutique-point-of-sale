rmdir /s/q build
python setup.py build
copy *.gif build\exe.win32-2.7
copy *.xlsx build\exe.win32-2.7
copy myicon.ico build\exe.win32-2.7
copy "Adine Kirnberg.ttf" build\exe.win32-2.7
mkdir  build\exe.win32-2.7\data
copy master_dir_external.txt build\exe.win32-2.7\data\master_dir.txt
copy BoutiquePOS_License.txt build\exe.win32-2.7\LICENSE.TXT
