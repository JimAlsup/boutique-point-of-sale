'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

# data format for excel number columns
FORMAT_CURRENCY_USD_SIMPLE = '"$"#,##0.00_-'

class PaidBy:
    cash = 1
    check = 2
    credit = 3
    # value 1..3
    # checkNum string value

    def __init__(self, value = None, checkNum = None):
        self.value = value or self.cash
        self.checkNum = checkNum or 0
        self.text = self.nameWithCheckNum()

    def get(self):
        return self.value

    def set(self, value):
        if value > self.cash and value <= self.credit:
            self.value = value
        self.text = self.nameWithCheckNum()

    def name(self, value = None):
        v = value or self.value
        names = ["cash", "check", "credit"]
        if v >= self.cash and v <= self.credit:
            return names[v-1]
        return "<invalid paidBy>"

    def nameWithCheckNum(self):
        n = self.name(self.value)
        if self.value == self.check and self.checkNum > 0:
            n += " #%04d" % self.checkNum
        return n

    def parse(self, input):
        method = input
        self.checkNum = 0
        if isinstance(method, basestring):
            if method[0:5] == self.name(self.check):
                self.value = self.check
                i = method.find("#")
                if i > 0:
                    try:
                        #print "parsing:", method[i+1:]
                        self.checkNum = int(method[i+1:])
                    except:
                        print "Invalid check number in paidBy arg to CustSale constructor: " + input
            elif method == "cash":
                self.value = self.cash
            elif method == self.name(self.credit):
                self.value = self.credit
            else:
                self.value = 0
                print "Invalid paidBy argument in CustSake Constructor: " + input
        else:
            self.value = input
        self.text = self.nameWithCheckNum()


class Sale:
    def __init__(self, artist, multiple, price, tax, refunded = None):
        self.artist = artist
        if isinstance(multiple, str):
            self.multiple = int(multiple)
        else:
            self.multiple = multiple
        if isinstance(price, str):
            self.price = float(price)
        else:
            self.price = price
        if isinstance(tax, str):
            self.tax = float(tax)
        else:
            self.tax = tax or 0.0
        self.refunded = refunded or ""

    def __str__(self):
        return "[sale:" + self.artist + ', ' + str(self.multiple) + "X " + str(self.price) + ', ' + str(self.tax)\
               + ', ' + self.refunded + "]"



class CustSale:
    totalTax = 0.00
    totalCost = 0.00


    def __init__(self, first=None, last=None, email=None, date=None, paidBy=None, checkNum=None):
        self.email = email or ""
        self.first = first or ""
        self.last = last or ""
        self.date = date or ""
        self.paidBy = PaidBy(paidBy or 0, checkNum or 0)
        self.sales = []  # list of sales records
        self.TransId = ""


    def __str__(self):
        r = "[cust:" + self.transId + ", " + self.first + ', ' + self.last + ', ' + self.email + ", " + self.date\
            + " paid by: " + self.paidBy.nameWithCheckNum() + "]"
        for s in self.sales:
            r += "\n  " + str(s)
        return r


    def setCheckNum(self, check):
        self.checkNum = check


    def addSale(self, artist, multiple, price, tax, refunded=None):
        refunded = refunded or False
        sale = Sale( artist, multiple, price, tax, refunded )
        self.sales.append(sale)


    def numberOfSales(self):
        return len(self.sales)


    def deleteSale(self, index):
        del self.sales[index]


    def subTotal(self):
        sub = 0
        for i in range(0,self.numberOfSales()):
            sub += self.sales[i].price * self.sales[i].multiple
        return round(sub,2)


    def tax(self):
        tax = 0
        for i in range(0,self.numberOfSales()):
            tax += self.sales[i].tax * self.sales[i].multiple
        return round(tax,2)

    def total(self):
        return round(self.subTotal() + self.tax(),2)



if __name__ == '__main__':
    # Unit test
    ci = CustSale("Jane", "Doe", "jane.doe@gmail.com", "10/11/16 11:00am", PaidBy.check, 451)
    ci.addSale("Custom Woodworks", "1", "10.00", "0.875")
    ci.addSale("Flowers for you", "2", "12.00", "1.10")
    ci.addSale("Flowers for you2","3",  "12.01", "1.10")
    ci.deleteSale(1)
    print ci
