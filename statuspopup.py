from Tkinter import *
import ttk

class StatusPopUp(object):
    def __init__(self, parent, reason, callback, x=None, y=None ):
        self._debug = False
        self.root = parent
        if x is None:
            self.x = self.root.winfo_rootx()+self.root.winfo_width()/2
        else:
            self.x = x
        if y is None:
            self.y = self.root.winfo_rooty()+self.root.winfo_height()/2
        else:
            self.y = y
        self.top = Toplevel(self.root)
        self.callback = callback
        self.reason = reason
        self.top.overrideredirect(True)
        self.top.protocol("WM_DELETE_WINDOW", self.top.quit())
        pos = "%+d%+d" % (x,y)
        self.top.geometry(pos)
        self.top.config(bd=2,relief=RAISED)
        row=Frame(self.top)
        row.pack(side=TOP)
        # print "d", self.sb.winfo_width(), self._dropXFactor, self.ffont.measure("a"), w
        self.lb = Listbox(row, width=65, height=5,
                          selectmode=SINGLE, bd=1, relief=GROOVE, activestyle='none',
                          state=NORMAL)
        self.lb.pack(side=LEFT)
        row=Frame(self.top)
        row.pack(side=TOP)
        self.close = Button(row, text="Close", command=self.top.quit, state=DISABLED)
        self.close.pack(side=BOTTOM)
        self.top.after(100,self._playTime)  #
        self.top.grab_set()
        self.top.mainloop()
        self.top.grab_release()
        if self._debug:
            print "StatusPop mainloop exiting"
        self.top.destroy()

    def addStatus(self, s, color=None):
        self.lb.insert(END, s)
        i = self.lb.size()-1
        if not color is None:
            self.lb.itemconfig(i, fg=color)
        self.top.update()

    # This function is called by StatusPop to allow background processing
    # This funciton must complete with result 0 to cause it to exit.
    # A > 0 result cause a minimum wait of result milliseconds before
    # test is called again
    def _playTime(self, count=None):
        if count is None:
            count = 0
        if count == 0:
            self.addStatus("Processing " + self.reason + "...", 'blue')
        while True:
            result = self.callback(self, count)
            if result == 0:
                break
            self.top.after( result, self._playTime, count+1)
            return  # allow for only one completed msg
        self.addStatus(self.reason + " completed", 'blue')
        self.top.bell()
        self.close.config(state=NORMAL)


# unit test code

# This function is called by StatusPop to allow background processing
# This funciton must complete with result 0 to cause it to exit.
# A > 0 result cause a minimum wait of result milliseconds before
# test is called again
def test( status, count ):
    assert count >= 0
    if count <= 1:
        status.addStatus("Stage %d..." % (count+1))
        for i in range(20000000):
            j = i + 1
        return 10
    return 0

def startIt():
    print "Starting StatusPop..."
    status = StatusPopUp(root, "Sale", test)
    print "Exiting StatusPop"


if __name__ == '__main__':
    root = Tk()
    root.resizable(0, 0)
    root.title( "Main Window" )
    row = Frame()
    row.pack(side=TOP)
    b = ttk.Button(row, text='Cycle', command=startIt)
    b.pack(side=LEFT)

    root.mainloop()
