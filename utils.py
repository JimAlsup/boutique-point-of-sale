'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

from Tkinter import *
import tkFont
import os

PATHSEP = '/'

def listDirs(dir):
    nlist = []
    if dir != "":
        try:
            list = os.listdir(dir) # returns list
        except:
            print "Database directory is invalid"
            return nlist
        for l in list:
            #print ">>", l
            if  not os.path.isfile(dir + "/" + l):
                if l[0] == '.':
                    continue   #  skip these
                nlist.append(l)
    return nlist


def findIndexINamesList(list, name):
    name = name.lower()
    i = 0
    for l in list:
        if l.lower() == name:
            return i
        i += 1
    return 0  # no match return default


# combbine up to 3 paths and return the result
def pathCombine(p1, p2, p3 = None):
    if p1 == "":
        return pathCombine(p2, p3)
    if p2 is None:
        return p1
    if p2 == "":
        if p3 is None or p3 == "":
            return p1
        p2 = p3
        p3 = None
    if p1[len(p1)-1] == '/':
        np = p1 + p2
    else:
        np = p1 + PATHSEP + p2
    if p3 == None:
        return np
    if np[len(np)-1] == '/':
        np = np + p3
    else:
        np = np + PATHSEP + p3
    return np

# If string is empty adjust it to be character at the end the ascii alphabet to
# allow search engines position it at the end (rather than the beginning.
def fix(s):
    if s == '':
        return '~'
    return s.lower()


class TitleDisplay():
    def __init__(self, window):
        self.window = window
        self.canvas = Canvas(window, width=10, height=40)
        self.canvas.pack(side=TOP, fill=X)
        self.script = tkFont.Font(family='Adine Kirnberg', size=26, weight='bold')

    def _horizontalGradientRectangle(self, x0, y0, x1, y1,
                                    startRed, startGreen, startBlue,
                                    endRed, endGreen, endBlue):
        x = x0
        while (x <= x1):
            # fade the three color components depending on the x coordinate
            # note that integer division works fine here: why?
            red = (startRed*(x1 - x) + endRed*(x - x0)) / (x1 - x0)
            green = (startGreen*(x1 - x) + endGreen*(x - x0)) / (x1 - x0)
            blue = (startBlue*(x1 - x) + endBlue*(x - x0)) / (x1 - x0)

            # create a hexadecimal color representation
            color = "#%02x%02x%02x" % (red, green, blue)

            # create a colored vertical line at position x
            self.canvas.create_line(x, y0, x, y1, fill=color)
            x +=1

    def showHeader(self, title):
        w = self.window.winfo_width()
        self._horizontalGradientRectangle(0, 0, w, 40,  255, 200, 0, 100, 80, 0)
        self.canvas.create_text(5, 21, anchor='w', font=self.script, text=title, fill="blue")

