'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

# pip install pypiwin32 to get the win32* imports used here

import tempfile
import sys
import os

if sys.platform == "win32":
    import win32api
    import win32print


def printFile(fileName):
    hInstance = win32api.ShellExecute(
      0,
      "print",
      fileName,
      #
      # If this is None, the default printer will
      # be used anyway.
      #
      '/d:"%s"' % win32print.GetDefaultPrinter(),
      ".",
      0 )
    if hInstance > 32:
        return ""
    return "%s (%d)", win32api.GetLastError(), hInstance

def printString(str):
    fileName = tempfile.mktemp(".txt")
    f = open(fileName, "w")
    f.write (str)
    f.close()
    print ">>", win32print.GetDefaultPrinter()

    # todo: switch to using ShellExecuteEx so can better
    #       monitor the result and delete the tmp file
    hInstance = win32api.ShellExecute(
        0,
        "print",
        fileName,
        #
        # If this is None, the default printer will
        # be used anyway.
        #
        '/d:"%s"' % win32print.GetDefaultPrinter(),
        ".",
        0 )

    # We cannot delete the file until it's done printing or queueing
    # print ">", os.remove(fileName)
    if hInstance > 32:
        return ""
    return "%s (%d)", win32api.GetLastError(), hInstance


if __name__ == '__main__':
    printString("Test of whatever")