'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

import tkMessageBox
from multicolumnlistbox import *
import salefile
import mail
import printit
import os
import statuspopup
import utils



def mlbSelectCmd(evt):
    updateFields()


def updateFields():
    if mlb.getSelectionIndex() >= 0:
        reportButton.config(state=NORMAL)
    else:
        reportButton.config(state=DISABLED)


def onComboSelect(evt):
    return

def factor(f):
    if f is None:
        return 0
    if type(f) is unicode:
        f = f.encode('ascii','ignore')
    if type(f) is str:
        f = f.strip()
        if len(f) == 0:
            return 0
        i = f.find('X')
        if i > 0:
            r = int(f[0:i])
        else:
            r = int(f)
    else:
        r = f
    return r


def myFloat(dstr):
    if dstr is None:
        return 0.0
    if type(dstr) is int or type(dstr) is long:
        return float(dstr)
    if type(dstr) is float:
        return dstr
    if type(dstr) is string:
        dollar = dstr.find('$')
        if dollar == -1:
            return float(dstr)
        return float(dstr[dollar+1:])


def createReport(aname):
    # if aname is "" do full report on all artist (e.g. boutique report)
    sf = salefile.SaleFile(et)
    wb, ws = sf.openDatabaseWorksheet(readOnly=True)
    if wb is None:
        return ""
    list = []  # lists of sale matching sales rows

    r = 0
    xfactor = 0
    rfactor = 0
    subTotal = 0.0
    subTax = 0.0
    haveSale = False
    haveRefund = False
    date = time.strftime("%m-%d-%Y %H:%M")
    msg =  "\nSales Report: " + et.getBoutiqueName() + " - " + et.getEventName() + \
           "      Generated on: " + date + "\n"
    fullReport = aname == ""
    if not fullReport:
        msg += "\nArtisan: %-25s\n" % aname
    for row in ws.rows:
        # tid = row[column=1].value
        r += 1
        t = row[0].value
        if t is None:
            t = ""
        t = t.strip()
         # strip off any trailing /TxxxxS field to avoid confusion
        if t != "" and t[0] == 'T':
            i = t.find('/')
            if i > 0:
                t = t[0:i]
            custRow = row  # throws aways previous record, no matches
            haveSale = t.find('S') >= 0
            haveRefund = t.find('R') >= 0
            continue
        if not (haveSale or haveRefund):
            continue
        a = row[1].value
        if a is None:
            custRow = None  # reset custRow
        else:
            # strip off the taxable string
            i = a.find("(taxable)")
            if i > 0:
                a = a[0:i]
        if (fullReport and a[0] != ' ') or aname == a:  # found one
            if not custRow is None:
                fullName = custRow[1].value + " " + custRow[2].value + ","
                msg += "\n  Transaction ID: %-13s  Date: %s  Paid By: %-12s\n" % \
                       (custRow[0].value, custRow[4].value, custRow[5].value)
                msg += "    Customer: %-25s  Email: %-20s\n" % (fullName, custRow[3].value)
                custRow = None
            xfactor = factor(row[2].value)
            price = myFloat(row[3].value)
            tax = myFloat(row[4].value)
            rfactor = factor(row[5].value)
            if haveSale:
                subTotal += price * (xfactor-rfactor)
                subTax += tax * (xfactor-rfactor)
                msg += "      Item(s) sold: "
            else:
                msg += "      Item(s) refunded: "
            if fullReport:
                if haveSale:
                    msg += "%-25s" % a
                else:
                    msg += "%-21s" % a
            msg += "%2dX, Price: $%7.2f, Tax: $%7.2f" % (xfactor, price, tax)
            if haveSale and rfactor > 0:
                msg += ", Returned: %2dX" % rfactor
            msg += "\n"
            # print "      ", a, xfactor, rfactor, price, subTotal, tax, subTax

            continue
        '''  alternative info line for refunds
        if custRow is None and haveRefund:  # we recorded a record if we got here
            i = a.find("Refund")
            if i >= 0 :
                i = a.find("(") + 1
                msg += "      Orig Sale Transaction ID: " + a[i:i+6] + "\n"
        '''

        # end for
    msg += "\n" + \
        "  Price Sub Total: $%7.2f\n" % subTotal + \
        "    Tax Sub Total: $%7.2f\n" % subTax + \
        "                   --------\n" + \
        "            Total: $%7.2f\n" % (subTotal + subTax)

    return msg
    # Now scan the worksheet looking for sales to the given artist

def doReportCallback(status, count):
    index = mlb.getSelectionIndex()
    status.addStatus("Generating report for %s..." % artists[index])
    # generate report
    msg = createReport(artists[index])
    # grab the email address (if any)
    v = al.findArtistInfo(artists[index])
    if not v is None:
        if emailInt.get() > 0:
            if len(v.email) > 0:
                status.addStatus("Emailing report to: " + v.email)
                result = mail.sendEMail(et.fromAddr, et.login, et.smtp, et.password, v.email,
                                        et.getBoutiqueName() + " - Sales Report", msg)
                if result != "":
                    tkMessageBox.showerror("Error", result, parent=top)
                    top.config(cursor="")
                    return False   # allow cashier to correct problem and retry
            else:
                status.addStatus("No email on file for " + artists[index])
    # Save the report to a file
    fn = utils.pathCombine(et.artistsEventDirectory(), artists[index]) + "_report.txt"
    status.addStatus("Saving report: "+fn)
    try:
        with open(fn, 'w') as f:
            f.write(msg)
    except IOError:
        status.addStatus("Unable to create artist report file", 'red' )
    # Print the report
    if printInt.get() != 0:
        result = printit.printFile(fn)
        if result != "":
            status.addStatus(result, 'red')
    done[index] = True
    mlb.tree.set(index + 1, column='#2', value="True")
    return 0


def doReport():
    if emailInt.get() == 0 and printInt.get() == 0:
        result = tkMessageBox.askyesno(
            "Warning", "Neither the email nor print option is selected.\n" +
            "Only the text file report will be generated in the event directory.\n" +
            "Do you wish to proceed?", parent=top)
        if not result:
            return
    index = mlb.getSelectionIndex()
    if index >= 0:
        if done[index] == True:
            result = tkMessageBox.askyesno(
                "Warning", "You have already done a report for this artist/vendor.\n" +
                "Do you wish to proceed?", parent=top)
            if not result:
                return

        # print "Starting StatusPop..."
        x = reportButton.winfo_rootx()+20
        y = reportButton.winfo_rooty()-50
        status = statuspopup.StatusPopUp(top, "Artisan Report", doReportCallback, x, y)
        # print "Exiting StatusPop"
    updateFields()
    return

def doFullReportCallback(status, count):
    status.addStatus("Creating full report")
    msg = createReport("")
    fn = utils.pathCombine(et.artistsEventDirectory(), "report.txt")
    status.addStatus("Writing report to: " + fn)
    try:
        with open(fn, 'w') as f:
            f.write(msg)
    except IOError:
        status.addStatus("Unable to create report file: " + fn, 'red')
    return 0

def doFullReport():
    # print "Starting StatusPop..."
    x = fullReportButton.winfo_rootx()+20
    y = fullReportButton.winfo_rooty()-50
    status = statuspopup.StatusPopUp(top, "Full Report", doFullReportCallback, x, y)
    # print "Exiting StatusPop"

def openEventDir():
    os.startfile(et.artistsEventDirectory())

    return


def onClose():
    top.quit()


# Create the New Sale UI form
def makeform(frame):
    global artists, artistsHeader
    global reportButton, fullReportButton
    global mlb
    global emailInt, printInt
    global done

    #script = tkFont.Font(family='Brush Script MT', size=18, weight='normal')
    script = tkFont.Font(family='Adine Kirnberg', size=26, weight='bold')

    bName = et.getBoutiqueName() + " - " + et.getEventName()
    th = utils.TitleDisplay(frame)

    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=0, pady=5)
    lab = Label(row, text="Select an Artist/Vendor to report on:", anchor='c')
    lab.pack(side=LEFT, padx=5, pady=0)

    # Read in the artists file and pull only names that are active
    # Now read in the event specific vendors file and mark any matches -
    # as paid vendors

    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=2, pady=5)
    al.readEventFile()
    artists, dummy1, dummy2, taxable = al.createLists(noDups=True, askParticipating=True)
    done = [False] * len(artists)
    artistsHeader = ['Artist/Vendor', "Done"]
    mlb = MultiColumnListbox(row, artistsHeader, 8)
    for i in range(len(artists)):
        try:
            mlb.tree.insert("", END, i+1, values=(artists[i], done[i]))
        except:
            print "Duplicate name found"
            # ignore
    mlb.tree.column(artistsHeader[0], width=200)
    mlb.tree.column(artistsHeader[1], anchor='c')
    emailInt = IntVar()
    emailInt.set(1)
    emailBox = Checkbutton(row, text="Email Report\nto Artisan", variable=emailInt, command=updateFields)
    emailBox.pack(side=TOP, padx=15, pady=5)
    printInt = IntVar()
    printInt.set(0)
    printBox = Checkbutton(row, text="Print Report ", variable=printInt, command=updateFields)
    printBox.pack(side=TOP, padx=15, pady=5)
    reportButton = ttk.Button(row, text=" Generate Artisan\nSpecific Report...", command=(lambda: doReport()),
                              state=DISABLED)
    reportButton.pack(side=TOP, padx=15, pady=5)

    fullReportButton = ttk.Button(row, text='Generate\nEvent Report...', command=(lambda: doFullReport()),
                              state=NORMAL)
    fullReportButton.pack(side=TOP, pady=5)
    mlb.tree.bind('<<TreeviewSelect>>', mlbSelectCmd)
    mlb.sortBy(mlb.getColumn())

    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=5, pady=5)
    browseEventDir = ttk.Button(row, text='  Browse Event Directory...  ', command=(lambda: openEventDir()),
                              state=NORMAL)
    browseEventDir.pack(side=TOP, pady=0)

     # line separator
    row = Frame(frame)
    row.pack(side=TOP, padx=5, pady=10, fill=X)
    separator = Frame(frame, height=5, bd=5, relief=SUNKEN)
    separator.pack(side=TOP, fill=X, padx=5, pady=5)

    # bottom button(s)
    row = Frame(frame)
    b = Button(row, text=' Close ', command=(lambda: onClose()))
    row.pack(side=BOTTOM, fill=X, padx=10, pady=5)
    b.pack(side=RIGHT)

    updateFields()
    frame.update()
    th.showHeader(bName)
    frame.grab_set()
    top.iconbitmap('myicon.ico')
    frame.mainloop()


def onClosing():
    top.quit()


def reports(frame, eventTracker, artistList):
    global top
    global al
    global et

    et = eventTracker
    al = artistList
    top = Toplevel(frame)
    top.resizable(0,0)
    #top.grab_set()
    top.focus_force()
    top.wm_title(et.appName() + ": Reports")
    top.protocol("WM_DELETE_WINDOW", onClosing)  # Handle the [x] event
    makeform(top)
    top.destroy()

