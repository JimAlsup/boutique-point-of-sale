'''

artistlist.py:

Reads in and maintains a data set of artist data in the an excel (.xlsx) file.
The data set must match in columns this format:
  artist, active, taxable, personal name, mail address, email address, home phone, cell phone
This file in only read, not written, thus more data columns can be added after cell phone #
The taxable field must have values Yes, No or Mixed.  The value Mixed means that two items artist
line items are created (one for untaxed items like food and one for taxed).  Note that an untaxed
artist lines does not neccessarily mean the item is not taxed.  The artist can asorbe  this
A line with a # starting in column A is consider a comment/documentation line

'''
import eventtrack
import tkMessageBox
from openpyxl import load_workbook
from openpyxl import Workbook


class Artist:
    def __init__(self, busName, taxable, email):
        self. busName = busName
        self.taxable = taxable
        self.email = email
        # for the configured event in question
        self.participating = "No"
        self.paid = "No"

    def __str__(self):
        return "[" + self.busName + "," + str(self.taxable) + "," + self.email + "]"


class ArtistList:
    def __init__(self, eventTrack):
        self.et = eventTrack
        self.vendors = []   # list of Artists (class)
        self.selectedEventOpen = True
        self.eventStateStr = ["Closed", "Open"]


    # return the number of artist in the list
    def count(self):
        return len(self.vendors)

    def __str__(self):
        s = "[ " + self.et.artistsListFileName() + ":\n"
        for a in self.vendors:
            s = s + "  " + a.__str__() + "\n"
        return s + "]"

    def fixUp(self, s):
        if s == None:
            return ""
        s = s.strip()
        if s == "":
            return ""
        if s[0] == '#':
            return ""
        return s


    # Read in the artist list (file given at construction time)
    def readFile(self, showError=None):
        self.showError = showError or False
        opened = False
        fn = self.et.artistsListFileName()

        try:
            wb = load_workbook(filename=fn, read_only=True)
            wb._archive.fp.close()  # work around bug in load_workbook.
                                    # Archive file is not properly closed - close it
            names = wb.get_sheet_names()
            ws = wb[names[0]]
        except IOError:
            print "Warning: Could not open/find artist vendor database file: ", fn
            if self.showError:
                tkMessageBox.showinfo("Warning",
                                      "The file below was not found:\n\n  " + fn + "\n\nIs the event " +
                                      "name/directory valid?\nThere should be a default artists.\n" +
                                      "Please check your installation.")
            return False

        # Scan through all the rows of the file
        rowNum = 0
        for rowX in ws.rows:
            rowNum += 1
            name = self.fixUp(rowX[0].value)
            if name == "":
                continue
            active = self.fixUp(rowX[1].value).lower()
            if active == "inactive":    # typically "inactive", but any value other than space
                continue
            taxName = self.fixUp(rowX[2].value).lower()   #  Yes, No or Mixed
            if taxName == "":
                print "Error - missing taxable field value in row:", rowNum, "file:", \
                    self.et.artistsListFileName
                continue
            email = self.fixUp(rowX[5].value)
            if taxName == "yes":
                taxable = True
            elif taxName == "no":
                taxable = False
            elif taxName == "mixed":
                artist = Artist(name+"(taxable)", True, email)
                self.vendors.append(artist)
                taxable = False
            else:
                print "Error - taxable field of:", taxName, "at row:", rowNum, " in file:", \
                    self.et.artistsListFileName, "is invalid"
                continue
            artist = Artist(name, taxable, email)
            self.vendors.append(artist)
        return self.count() > 0

    def createLists(self, noDups=None, askParticipating=None):
        participating = askParticipating or False
        noDups = noDups or False
        list = []
        part = []
        paid = []
        taxable = []
        for v in self.vendors:
            # print v.busName, v.participating
            if noDups and v.busName.find("(taxable)") != -1:   # rule out duplicates
                continue
            if participating and v.participating == "No":  # rule out non participants if asked
                continue
            list.append(v.busName)
            part.append(v.participating)
            paid.append(v.paid)
            taxable.append(v.taxable)
        return list, part, paid, taxable

    def findArtistInfo(self, name):
        for v in self.vendors:
            if v.busName == name:
                return v
        return None

    # The event directory contains an artist file that identifies the
    # artist participating in the event and whether they have paid or not
    # read it in and
    def readEventFile(self, showError=None):
        self.showError = showError or False

        for i in range(len(self.vendors)):
            self.vendors[i].participating = "No"
            self.vendors[i].paid = "No"

        fn = self.et.eventArtistsFileName()
        self.selectedEventOpen = True

        try:
            wb = load_workbook(filename=fn, read_only=True)
            wb._archive.fp.close()  # work around bug in load_workbook.
                                    # Archive file is not properly closed - close it
            names = wb.get_sheet_names()
            ws = wb[names[0]]
        except IOError:
            print "Error: Could not open/find artist vendor database file:\n  ", fn
            if self.showError:
                tkMessageBox.showinfo("No Artist/Vendor Event File",
                                      "The file below was not found:\n\n  " + fn + "\n\nIs the event " +
                                      "name/directory valid?\nPlease fix before proceeding.")
            return False
        for rowX in ws.rows:
            name =  self.fixUp(rowX[0].value)
            if name == "":
                continue
            if name[0] == '#':
                continue
            if name == self.eventStateStr[int(True)]:
                self.selectedEventOpen = True
                continue
            if name == self.eventStateStr[int(False)]:
                self.selectedEventOpen = False
                continue
            part = self.fixUp(rowX[1].value)
            paid = self.fixUp(rowX[2].value)
            # Scan the main artist list for
            found = False
            for i in range(len(self.vendors)):
                if self.vendors[i].busName.lower().find(name.lower()) == 0:
                    self.vendors[i].participating = part
                    self.vendors[i].paid = paid
                    found = True
                    # cannot break here as we need to match the possible tax version as well
            if not found:
                print "Warning: event artists entry: ", name, "was not found in main file."
        return True

    def updateEventFile(self, showError=None):
        self.showError = showError or False
        fn = self.et.eventArtistsFileName()

        try:
            wb = load_workbook(filename=fn)
            names = wb.get_sheet_names()
            ws = wb[names[0]]
        except IOError:
            try:
                wb = Workbook()  # Create a new file
                ws = wb.active
                ws.title = "Sheet1"
                ws.cell(row=1, column=1).value = "# Artist/Vendors event specific info:  DO NOT EDIT THIS FILE"
                ws.column_dimensions["A"].width = 30   # artist/vendor
            except:
                print "Error: Could not open/create artist vendor database file:\n  ", fn
                if self.showError:
                    tkMessageBox.showinfo("Artist/Vendor Event File",
                                          "The file below could not be open/created:\n\n  " + fn +
                                          "\n\nIs the event " +
                                          "name/directory valid?\nPlease fix before proceeding.")
                return False
        ws.cell(row=2, column=1).value = self.eventStateStr[self.selectedEventOpen]
        r = 3  # excel row number
        for v in self.vendors:
            if v.busName.find("(taxable)") != -1:
                # print v.busName, v.taxable
                continue
            ws.cell(row=r, column=1).value = v.busName
            ws.cell(row=r, column=2).value = v.participating
            ws.cell(row=r, column=3).value = v.paid
            r += 1
        wb.save(fn)


if __name__ == '__main__':
    et = eventtrack.EventTrack()
    al = ArtistList(et)
    print al.readFile()
    print al
