'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

from Tkinter import *
import ttk
import tkFont
import time
import string
import utils

SECONDS_FOR_KEY_TIME_OUT = 5 * 1000  # five seconds search timeout

class MultiColumnListbox(object):
    """use a ttk.TreeView as a multicolumn ListBox"""

    # headers
    def __init__(self, frame, headers, lines, escFunc=None, returnFunc=None):
        self.escFunc = escFunc or self.escKey
        self.returnFunc = returnFunc or self.returnKey
        self.col = headers[0]  # default sort column
        self.searchStr = ""
        self.start = 0  #timer used in idle character input
        self.tree = None
        self.descending = True
        self.mode = []
        self.headers = headers
        self.lines = lines
        self.container = frame
        self.activeIdle = 0
        self.sizeIt = 0
        self._setupWidgets()
        self._buildTree()


    def getColumn(self):
        return self.col

    def _setupWidgets(self):
        # s = "Click on header to sort by that column to change width of column drag boundary"
        # create a treeview with vertical scrollbar
        self.vsb = ttk.Scrollbar(self.container, orient="vertical")
        self.tree = ttk.Treeview(self.container, height=self.lines, columns=self.headers,
                                 selectmode=BROWSE, show="headings", yscrollcommand=self.vsb.set)
        self.vsb.config(command=self.tree.yview)
        self.tree.pack(side=LEFT, fill=X)
        self.vsb.pack(side=LEFT, fill=Y)
        self.tree.bind("<Key>", self._key)
        self.searchBoxStr = StringVar()
        self.searchBox = Label(self.container, width=5, text="", borderwidth=0, relief="solid",
                               textvariable=self.searchBoxStr, fg='dark grey')
        # searchBox is place later dynamically
        self.image_up = PhotoImage(file="navigate-up.gif")
        self.image_down = PhotoImage(file="navigate-down.gif")
        self.image_blank = PhotoImage(file="navigate-blank.gif")



    def _buildTree(self):
        for col in self.headers:
            self.tree.heading(col, text=col.title(),
                command=lambda c=col: self.sortBy(c))
            # adjust the column's width to the header string
            self.tree.column(col, width=tkFont.Font().measure(col.title())+10)
            self.mode.append(False)


    # Returns the selected index value (0 based) (-1 for no selected).
    # This only works with a single selection (BROWSE)
    # Implementation note:
    #   The retuned value represents the 0 based row index which must be specified at insertion time
    #   It should be used to index into your data store outside of this multicolumnlistbox
    def getSelectionIndex(self):
        # print ">>>", self.tree.selection()[0]
        list = self.tree.selection()
        if len(list) == 0:
            return -1
        return int(list[0]) - 1

    # This functions can be overridden to change behavior or a function pointer passed at
    # init time can be called instead.  The default behavior is to quit the enclosing
    # mainloop allowing the current tree selection to be queried and used
    def returnKey(self):
        self.container.quit()

    # This functions can be overridden to change behavior or a function pointer passed at
    # init time can be called instead.  The default behavior is to quit the enclosing
    # mainloop after deselecting any selections
    def escKey(self):
        # print "<esc>"
        list = self.tree.selection()
        for l in list:
            self.tree.selection_remove(l)
        self.container.quit()

    def _key(self, event):
        # print "key:", repr(event.char),
        #if len(self.tree.selection()) == 0:
        #    self.searchBox.place_forget()
        #    self.container.bell()
        #    return
        curTime =  time.clock()
        if self.start == 0:
            self.start = curTime
        elif curTime-self.start > 2:  # expired - (2 seconds, a float can be used
            # print "reset time"
            self.searchStr = ""
            self.start = 0
        self.start = curTime
        if event.char == '\r':
            self.returnFunc()  # does not return unless user supplied
            return
        if event.char == '\x1b':
            self.escFunc()   # does not return unless user supplied
            return
        bksp = event.char == '\x08'
        if bksp or event.char in string.letters + string.digits + ' ':
            if bksp:
                self.searchStr = self.searchStr[0:len(self.searchStr)-1]
            else:
                self.searchStr += event.char
            # print "["+self.searchStr+"]"
            # grab values to sort
            found = 0
            if self.searchStr == "":
                self.searchBox.place_forget()
            else:
                self.searchBoxStr.set(self.searchStr)
                xpos = self.tree.winfo_width() - (tkFont.Font().measure("     ") + 8)
                self.searchBox.place(x=xpos, y=self.tree.winfo_height()-19)
                self.tree.after(SECONDS_FOR_KEY_TIME_OUT, self._onIdle)
                self.activeIdle += 1

                data = [(self.tree.set(child, self.col), child) \
                    for child in self.tree.get_children('')]
                s = self.searchStr.upper()
                found = ""
                for value in data:
                    if s == value[0].upper()[0:len(self.searchStr)]:
                        found = value[1]
                        break
                if found == "":
                    self.container.bell()
                # print "}}", found, value[0]
            if found != 0:
                self.tree.see(found)
                self.tree.selection_set(found)
                self.tree.focus(found)

    def _onIdle(self):
        # print "Got it"
        self.activeIdle -= 1
        if self.activeIdle <= 0:
            self.activeIdle = 0
            self.searchBox.place_forget()
            self.searchBoxStr.set("")


    def focusSelectTop(self):
        # get a tuple of the top level items (all of them in our case)
        # the first one will be the one we want (expensive - todo: research better method
        list = self.tree.get_children()
        if len(list) > 0:
            index = str(list[0])
            # set the selection item and focus to the top item shown
            self.tree.selection_set(index)
            # self.tree.focus_set()   # set focus to the tree itself - makes keyboard work
            self.tree.focus(index)
            self.tree.focus_force()
            # print "focus>>", index



    def sortBy(self, col, descending=None):
        """sort tree contents when a column header is clicked on"""
        # grab values to sort
        # if changing columns blank out the indicator
        try:
            index = self.headers.index(col)
        except:
            index = 0
        if self.col != col:
            self.tree.heading(self.col, image=self.image_blank)
            # restore that last mode for that column
            self.descending = self.mode[index]
        else:
            if descending == None:
                self.descending = not self.descending
            else:
                self.descending = descending
            self.mode[index] = self.descending

        # print self.col, col
        self.col = col
        data = [(utils.fix(self.tree.set(child, col)), child) \
            for child in self.tree.get_children('')]
        # if the data to be sorted is numeric change to float
        # now sort the data in place
        # newdata = sorted( data, reverse=self.descendin )
        data.sort(reverse=self.descending)
        for ix, item in enumerate(data):
            self.tree.move(item[1], '', ix)
        # switch the heading so it will sort in the opposite direction
        self.tree.heading(col, command=lambda col=col: self.sortBy(col))
        if self.descending:
            self.tree.heading(col, image=self.image_up)
        else:
            self.tree.heading(col, image=self.image_down)


def cancel():
    selected_index = listbox.getSelectionIndex()
    root.quit()


if __name__ == '__main__':
    car_header = ['    car    ', '    repair    ']
    car_list = [
    ('Hyundai', 'brakes') ,
    ('Honda', 'light') ,
    ('Lexus', 'battery') ,
    ('Benz', 'wiper') ,
    ('Ford', 'tire') ,
    ('Chevy', 'air') ,
    ('Chrysler', 'piston') ,
    ('Toyota', 'brake pedal') ,
    ('BMW', 'seat')
    ]

    root = Tk()
    root.title("Multicolumn Treeview/Listbox")
    row = Frame()
    row.pack(side=TOP, fill=X)
    listbox = MultiColumnListbox(row, car_header, len(car_list))
    for item in car_list:
        listbox.tree.insert('', 'end', values=item)
    listbox.focusSelectTop()

    row = Frame()
    row.pack(side=TOP, fill=X, padx=5, pady=2)
    b1 = Button(row, text=' Cancel ', command=cancel)
    b1.pack(side=LEFT, padx=10, pady=10)

    root.mainloop()


