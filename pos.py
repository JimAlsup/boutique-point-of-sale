'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

from Tkinter import *
from idlelib.ToolTip import *
import tkMessageBox
import tkFileDialog
import ttk
import tkFont
import shutil, errno
import os
import utils
import sale
import refund
import admin
import eventtrack

Version = "0.0.1"

class MyButton(ttk.Frame):
    def __init__(self, parent, height=None, width=None, text="", command=None, style=None):
        ttk.Frame.__init__(self, parent, height=height, width=width, style="MyButton.TFrame")

        self.pack_propagate(0)
        self._btn = ttk.Button(self, text=text, command=command, style=style)
        self._btn.pack(fill=BOTH, expand=1)


# Review and cancel a sale or refund an item
def cancelSale(window, et):
    window.withdraw()
    refund.refundSale(et)
    window.deiconify()

# Wrapper around newSale call, needed to hide called from window and bring it back after closing
# Failing to do this leaves the root window open and still accessible which allows for
# mulitple new sales windows and that will cause data problems
def newSale(window, et):
    window.withdraw()
    sale.newSale(et)
    window.deiconify()


def kill():
    print "App killed"
    root.destroy()

def updateScreen():
    if os.path.exists(et.topLevelDir()):
        dir.config(bg='white')
    else:
        dir.config(bg='tomato')
    sel = et.selectedEventIndex()
    event.config(values=et.events())
    if sel >= 0:
        event.current(sel)
    else:
        event.set("")
    if event.current() == -1:
        saleButton.config(state=DISABLED)
        refundButton.config(state=DISABLED)
    else:
        saleButton.config(state=NORMAL)
        refundButton.config(state=NORMAL)
    if bNameStr.get().strip() != et.getBoutiqueName().strip():
        bNameStr.set(et.getBoutiqueName())
        th.showHeader(bNameStr.get())


def onComboSelect(evt):
    # print "}}", et._events
    # print "}}}", event.current(), et._events[event.current()]
    sel = event.current()
    et.setEventIndex(sel)
    updateScreen()

def getDirectory(window):
    while True:
        dirname = tkFileDialog.askdirectory(parent=window, initialdir=et.topLevelDir(),
                                            title='Please select a directory')
        if dirname == "":
            return
        # print ">>", dirname
        if len(dirname ) > 0:
            if os.path.exists(dirname):
                break
            tkMessageBox.showinfo("Sorry", "The selected directory:\n\n  " + dirname + \
                                  "\n\ndoes not exist - please select a valid directory", parent=window)

    dst = utils.pathCombine(dirname, et._artistListFileName)
    if not os.path.exists(dst):
        # if new directory then warn the user and if they continue then create the stuff
        #   - copy templates (artists.xlsx, clients.xlsx) from program loc to configured directory
        # misc_data
        result = tkMessageBox.askokcancel("Warning", "The directory:\n\n  %s\n\ndoes not contain a POS database.\n" % dirname + \
                                  "\n\nShall we create a new one or cancel?", parent=window)
        if not result:
            return
        try:
            shutil.copy(et.artistListTemplateFileName, dst)
        except:
            tkMessageBox.showerror("Error", "Unable to copy %s from program directory to:\n\n  %s."
                                   % (et.artistListTemplateFileName,  dst), parent=window)
    dst = utils.pathCombine(dirname,et._customerDatabaseFileName)
    if not os.path.exists(dst):  # test it in case it already exists
        try:
            shutil.copy(et.customerDatabaseTemplateFileName, dst)
        except:
            tkMessageBox.showerror("Error", "Unable to copy %s from program directory to:\n\n  %s."
                                   % (et.customerDatabaseTemplateFileName,  dst), parent=window)
    et.updateMasterFile(dirname)
    dirStr.set(dirname)
    dir.config(bg='white')
    updateScreen()


def adminCall():
    admin.admin(root, et)
    dirStr.set(et.topLevelDir())
    updateScreen()
    root.focus_force()



# Main entry point for program
if __name__ == '__main__':
    global dirStr
    global saleButton, refundButton
    global bNameStr
    global th

    root = Tk()
    # root.tk.call('tk', 'scaling', 1.5)

    script = tkFont.Font(family='Adine Kirnberg', size=26, weight='bold')

    et = eventtrack.EventTrack()
    root.resizable(0, 0)
    root.title( et.appName() + " " + Version )
    #row = Frame()
    bNameStr = StringVar()
    bNameStr.set(et.getBoutiqueName())

    th = utils.TitleDisplay(root)

    # row = Frame()
    # lab = Label(row, textvariable=bNameStr, anchor='w', font=script, fg="blue", bg="cyan")
    # row.pack(side=TOP, fill=X, padx=0, pady=0)
    # lab.pack(side=LEFT, fill=X, expand=Y)

    row = Frame()
    row.pack(side=TOP, fill=X, padx=2, pady=10)
    lab = Label(row, text="Database Location:", anchor='w')
    lab.pack(side=LEFT, padx=5)
    dirStr = StringVar()
    dir = Label(row, width=43, borderwidth=1, relief=GROOVE, bg="white",
                textvariable=dirStr, anchor='w')
    dir.pack(side=LEFT,padx=5)
    dirStr.set(et.topLevelDir())
    tip = ToolTip(dir, "Directory location of boutique database")
    if not os.path.exists(et.topLevelDir()):
        dir.config(bg='tomato')
    b1 = MyButton(row, text='...', height=20, width=30, command=(lambda: getDirectory(root)))
    b1.pack(side=LEFT, padx=5)
    tip = ToolTip(b1, "Browse to database directory")

    row = Frame()
    row.pack(side=TOP, fill=X, padx=10, pady=10)
    lab = Label(row, text="Event:", anchor='w')
    lab.pack(side=LEFT, padx=5)
    event = ttk.Combobox(row, width=20, state='readonly', foreground='blue')
    event.bind("<<ComboboxSelected>>", onComboSelect)
    event.pack(side=LEFT)
    saleButton = ttk.Button(row, text='  New Sale...  ', command=(lambda: newSale(root,et)))
    saleButton.pack(side=LEFT, padx=15)
    tip = ToolTip(saleButton, "Make a new sale")
    refundButton = ttk.Button(row, text='  Refund Sale...  ', command=(lambda: cancelSale(root,et)))
    tip = ToolTip(refundButton, "Review or refund a sale")
    refundButton.pack(side=LEFT, padx=0)

    separator = Frame(height=5, bd=5, relief=SUNKEN)
    separator.pack(fill=X, padx=5, pady=10)

    row = Frame()
    b2 = ttk.Button(row, text='Admin', command=(lambda: adminCall()))
    tip = ToolTip(b2, "Launch Administration Dialog")
    b3 = ttk.Button(row, text='Quit', command=root.quit)
    lab = Label(row, text="(Only use \"Admin\" when one terminal is active)", anchor='w')
    row.pack(side=TOP, fill=X, padx=10, pady=10)
    b2.pack(side=LEFT, padx=5)
    lab.pack(side=LEFT, padx=0)
    b3.pack(side=RIGHT)

    updateScreen()
    root.update()
    th.showHeader(bNameStr.get())

    root.wm_protocol("WM_DELETE_WINDOW", kill)
    if et.topLevelDir() == '.':
        tkMessageBox.showinfo("Warning", "./ should not be used as a database directory.\n" +
                     "Please use the browse (...) button to create or select a directory",
                     parent=root)
    if et.topLevelDir() == '':
        tkMessageBox.showinfo("Warning", "Database directory has not been created or selected.\n" +
                     "Please use the browse (...) button to create or select a directory.",
                     parent=root)

    root.iconbitmap('myicon.ico')
    root.mainloop()

