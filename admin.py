'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

# from Tkinter import *
from idlelib.ToolTip import *
import tkMessageBox
import os
import artistlist
from multicolumnlistbox import *
import clients
import reports


def mlbSelectCmd(evt):
    selectedListItem = mlb.getSelectionIndex()  # return the zero based data index
    print "Select", selectedListItem, evt


def mlbDoubleClick(evt):
    global mlbChanged
    selectedListItem = mlb.getSelectionIndex()  # return the zero based data index
    if selectedListItem == -1:
        return
    if closed.get() == 1:
        tkMessageBox.showinfo("Sorry", "Event is closed to changes.\n" +
                              "Reopen it to make modifications.", parent=top)
        return
    col = mlb.tree.identify_column(evt.x)
    if selectedListItem < len(artists):
        if col == "#2":
            if participating[selectedListItem] == "Yes":
                participating[selectedListItem] = "No"
            else:
                participating[selectedListItem] = "Yes"
            mlb.tree.set(selectedListItem+1, column=col, value=participating[selectedListItem])
            mlbChanged = True
            updateFields()
        elif col == "#3":
            if paid[selectedListItem] == "Yes":
                paid[selectedListItem] = "No"
            else:
                paid[selectedListItem] = "Yes"
            mlb.tree.set(selectedListItem+1, column=col, value=paid[selectedListItem])
            mlbChanged = True
            updateFields()

def updateFields():
    global lastComboSel
    if not doUpdates:
        return
    bNameStr.set(et.getBoutiqueName() +  " - " + et.getEventName())
    th.showHeader(bNameStr.get())
    if len(newEventStr.get()) > 0:
        newEventButton.config(state=NORMAL)
    else:
        newEventButton.config(state=DISABLED)
    emailChanged = fromAddrStr.get() != et.fromAddr or loginStr.get() != et.login or \
         smtpStr.get() != et.smtp or passwordStr.get() != et.password
    if taxRateStr.get() != savedTaxRate or et.getBoutiqueName() != boutiqueStr.get()\
            or emailChanged or et.allowPrintReceipt != prInt.get():
        applyButton.config(state=NORMAL)
    else:
        applyButton.config(state=DISABLED)
    sel = et.selectedEventIndex()
    if lastComboSel == -1:
        lastComboSel = sel
    event.config(values=et.events())
    if sel >= 0:
        event.current(sel)
    else:
        event.set("")
    if mlbChanged or closed.get() != int(not al.selectedEventOpen):
        apply2Button.config(state=NORMAL)
    else:
        apply2Button.config(state=DISABLED)
    if event.current() != -1:
        report.config(state=NORMAL)
    else:
        report.config(state=DISABLED)


def updateArtistList(sel):
    global artists, lastComboSel, mlbChanged
    # delete the current set of sale items and repopulate from selected transaction
    for i in range(len(artists)):
        mlb.tree.delete(i+1)

    # get the new data
    et.setEventIndex(sel)
    al.readEventFile()
    artists, participating, paid, dummy = al.createLists(noDups=True)

    for i in range(len(artists)):
        try:
            mlb.tree.insert("", END, i+1, values=(artists[i], participating[i], paid[i]))
        except:
            print "Duplicate artist in event list found, ignored:", i, artists[i]
    mlb.sortBy("#1", False)
    closed.set(int(not al.selectedEventOpen))
    mlbChanged = False
    lastComboSel = sel


def createNewEvent():
    name = newEventStr.get()
    err = et.createEvent(name)
    if err != "":
        tkMessageBox.showinfo("Error:", err, parent=top)
        return
    updateArtistList(et.selectedEventIndex())
    newEventStr.set("")
    updateFields()
    clients.insertEventInWorksheet(et, top, et.customerDatabaseFileName(), 4, name)
    tkMessageBox.showinfo("Information", "Event successfully created!", parent=top)


def apply(window):
    print "apply"
    et.fromAddr = fromAddrStr.get()
    et.login    = loginStr.get()
    et.smtp     = smtpStr.get()
    et.password = passwordStr.get()
    try:
        value = float(taxRateStr.get()) / 100
        et.taxRate = value
    except:
        print "Error: tax rate invalid and ignored."
        tkMessageBox.showinfo("Warning",  "Edited tax rate is invalid!\nNo change made.",
                              parent=window)
    et.setBoutiqueName(boutiqueStr.get())
    et.allowPrintReceipt = prInt.get()
    et.updateMiscFile()
    updateFields()


def apply2(window):
    global mlbChanged
    update = False
    if closed.get() != int(not al.selectedEventOpen):
        al.selectedEventOpen = not bool(closed.get())
        update = True
    if mlbChanged:
        # update the paid and participating values in the artist structures
        for i in range(len(al.vendors)):
            for a in range(len(artists)):
                if al.vendors[i].busName == artists[a]:
                    al.vendors[i].participating = participating[a]
                    al.vendors[i].paid = paid[a]
                    break
    if update or mlbChanged:
        al.updateEventFile()
        mlbChanged = False
    updateFields()


def onComboSelect(evt):
    global artists, participating, paid
    global mlbChanged
    global lastComboSel

    sel = event.current()
    if sel == lastComboSel:
        print "Bypassed"
        return
    if mlbChanged or closed.get() != int(not al.selectedEventOpen):  # unsaved changes
        result = tkMessageBox.askokcancel(
            "Warning",  "You have unsaved changes (participating, paid, or open/closed).\n\n" +
            "Select \"OK\" to discard changes or Select \"Cancel\" to abort event change.",
             parent=top)
        if not result:
            # revert event change - need to revert selection back to previous
            event.current(lastComboSel)
            return
    updateArtistList(sel)
    updateFields()


def doReports(window):
    reports.reports(window, et, al)
    window.focus_force()


def onClose(window):
    # print ">>", newEventStr.get(), mlbChanged, closed.get(), int(not al.selectedEventOpen)
    if taxRateStr.get() != savedTaxRate or et.getBoutiqueName() != boutiqueStr.get() or\
        len(newEventStr.get()) > 0 or  mlbChanged or closed.get() != int(not al.selectedEventOpen):
        result = tkMessageBox.askokcancel(
            "Warning",  "You have unsaved changes.\n\n" +
            "Select \"OK\" to abandon changes and exit.\nSelect \"Cancel\" to return.",
             parent=window)
        if not result:
            return
    window.quit()


def editArtistans():
    os.startfile(et.artistsListFileName())
    tkMessageBox.showinfo("Warning", "If you have made changes to the artisans file please close\n"
                           "this admin screen and reload it", parent=top)

# Create the New Sale UI form
def makeform(frame):
    global dir, dirStr
    global refundXCol, refund, prevXCol, saleXCol
    global refundPCol, refundTCol
    global boutiqueStr, bNameStr
    global th
    global newEventButton, newEventStr
    global taxRateStr
    global applyButton
    global savedTaxRate
    global closed
    global event
    global mlb
    global artists, participating, paid, artistsHeader
    global mlbChanged, apply2Button
    global lastComboSel
    global report
    global doUpdates
    global fromAddrStr
    global loginStr
    global smtpStr
    global passwordStr
    global printReceipt, prInt

    doUpdates = False

    bNameStr = StringVar()
    bNameStr.set(et.getBoutiqueName() + " - " + et.getEventName())
    th = utils.TitleDisplay(frame)

    row = Frame(frame)
    row.pack(side=TOP, fill=X, pady=0)
    separator = Frame(frame, height=0, bd=5)
    separator.pack(side=TOP, fill=X, padx=0, pady=5)

    row = Frame(frame)
    row.pack(side=TOP, fill=X, pady=0)
    lab = Label(row, text="Database Location:", anchor='e')
    lab.pack(side=LEFT, padx=10, pady=5)
    dirStr = StringVar()
    dir = Label(row, width=50, borderwidth=1, relief=GROOVE, bg="white",
                textvariable=dirStr, anchor='w')
    dir.pack(side=LEFT, padx=5)
    dirStr.set(et.topLevelDir())

    # line separator
    row = Frame(frame)
    row.pack(side=TOP, pady=0, fill=X)
    separator = Frame(frame, height=3, bd=5, relief=SUNKEN)
    separator.pack(side=TOP, fill=X, padx=25, pady=10)

    # boutique name and tax rate
    row = Frame(frame)
    row.pack(side=TOP, fill=X, pady=5)
    lab = Label(row, text="Boutique Name:", anchor='w')
    lab.pack(side=LEFT, padx=10)
    boutiqueStr = StringVar()
    boutiqueStr.set(et.getBoutiqueName())
    boutiqueStr.trace("w", lambda name, index, sv=boutiqueStr: updateFields())
    boutique = Entry(row, width=35, textvariable=boutiqueStr)
    boutique.pack(side=LEFT, padx=0)
    tip = ToolTip(boutique, "Enter your boutique or craft fair name")
    lab = Label(row, text="Tax Rate:", anchor='w')
    lab.pack(side=LEFT, padx=5)
    taxRateStr = StringVar()
    taxRateStr.set(round(et.taxRate*100, 2))
    savedTaxRate = taxRateStr.get()
    taxRateStr.trace("w", lambda name, index, sv=taxRateStr: updateFields())
    tax = Entry(row, width=6, textvariable=taxRateStr, justify='right')
    tax.pack(side=LEFT, padx=0)
    but = ttk.Button(row, text='Edit Artisans...', command=editArtistans)
    but.pack(side=LEFT, padx=20)
    tip = ToolTip(but, "Launches Excel to edit Artisan data")

    row = Frame(frame)
    row.pack(side=TOP, pady=0, fill=X)
    prInt = IntVar()
    prInt.set(int(et.allowPrintReceipt))
    printReceipt = Checkbutton(row, variable=prInt, command=updateFields,
                               text="Allow printing of sale/refund receipts")
    printReceipt.pack(side=LEFT, padx=10)


    row = Frame(frame)
    row.pack(side=TOP, pady=5, fill=X)
    lab = Label(row, width=11, text="Email Setup:", anchor='e', fg='blue')
    lab.pack(side=LEFT, padx=0)
    lab = Label(row, width=11, text="From Address:", anchor='e')
    lab.pack(side=LEFT, padx=0)
    fromAddrStr = StringVar()
    fromAddrStr.trace("w", lambda name, index, sv=fromAddrStr: updateFields())
    fromAddr = Entry(row, width=22, textvariable=fromAddrStr)
    fromAddr.pack(side=LEFT, padx=0)
    lab = Label(row, width=9, text="Login:", anchor='e')
    lab.pack(side=LEFT, padx=0)
    loginStr = StringVar()
    loginStr.trace("w", lambda name, index, sv=loginStr: updateFields())
    login = Entry(row, width=15, textvariable=loginStr)
    login.pack(side=LEFT, padx=0)

    row = Frame(frame)
    row.pack(side=TOP, pady=0, fill=X)
    lab = Label(row, width=23, text="SMTP Server:", anchor='e')
    lab.pack(side=LEFT, padx=0)
    smtpStr = StringVar()
    smtpStr.trace("w", lambda name, index, sv=smtpStr: updateFields())
    smtp = Entry(row, width=22, textvariable=smtpStr)
    smtp.pack(side=LEFT, padx=0)
    lab = Label(row, width=9, text="Password:", anchor='e')
    lab.pack(side=LEFT, padx=0)
    passwordStr = StringVar()
    passwordStr.trace("w", lambda name, index, sv=passwordStr: updateFields())
    password = Entry(row, width=15, textvariable=passwordStr, show='*')
    password.pack(side=LEFT, padx=0)
    applyButton = ttk.Button(row, text='Apply', command=(lambda: apply(frame)), state=DISABLED)
    applyButton.pack(side=LEFT, padx=10, pady=10)
    lab = Label(row, width=3, text="", anchor='w')
    lab.pack(side=RIGHT, padx=0)
    fromAddrStr.set(et.fromAddr)
    loginStr.set(et.login)
    smtpStr.set(et.smtp)
    passwordStr.set(et.password)

    # line separator
    row = Frame(frame)
    row.pack(side=TOP, pady=0, fill=X)
    separator = Frame(frame, height=3, bd=5, relief=SUNKEN)
    separator.pack(side=TOP, fill=X, padx=30, pady=10)

    row = Frame(frame)
    row.pack(side=TOP, fill=X, pady=10)
    lab = Label(row, text="New Event Name:", anchor='w')
    lab.pack(side=LEFT, padx=10)
    newEventStr = StringVar()
    newEventStr.set("")
    newEventStr.trace("w", lambda name, index, sv=newEventStr: updateFields())
    newEvent = Entry(row, width=25, textvariable=newEventStr)
    newEvent.pack(side=LEFT, padx=5)
    newEventButton = ttk.Button(row, text='Create New Event...', command=createNewEvent, state=DISABLED)
    newEventButton.pack(side=LEFT, padx=15)

    row = Frame(frame)
    row.pack(side=TOP, pady=0, fill=X)
    separator = Frame(frame, height=3, bd=5, relief=SUNKEN)
    separator.pack(side=TOP, fill=X, padx=30, pady=10)

    row = Frame(frame)
    row.pack(side=TOP, fill=X, pady=10)
    lab = Label(row, text="Event:", anchor='w')
    lab.pack(side=LEFT, padx=10)
    event = ttk.Combobox(row, width=20, state='readonly', foreground='blue')
    event.bind("<<ComboboxSelected>>", onComboSelect)
    event.pack(side=LEFT)
    lastComboSel = -1
    lastComboSel = event.current()
    closed = IntVar()
    closeButton = Checkbutton(row, text="Event Closed:", variable=closed, command=updateFields)
    #closeButton.select()
    closeButton.pack(side=LEFT, padx=5)
    report = ttk.Button(row, text='  Event Reports...  ', command=(lambda: doReports(frame)))
    report.pack(side=LEFT, padx=10)

    # Read in the artists file and pull only names that are active
    # Now read in the event specific vendors file and mark any matches -
    # as paid vendors

    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=20, pady=5)

    al.readEventFile()
    artists, participating, paid, dummy = al.createLists(noDups=True)
    # print artists
    closed.set(int(not al.selectedEventOpen))

    artistsHeader = ['Artist/Vendor                 ',
                     'Participating',
                     ' Paid ']
    mlb = MultiColumnListbox(row, artistsHeader, 6)
    for i in range(len(artists)):
        try:
            mlb.tree.insert("", END, i+1, values=(artists[i], participating[i], paid[i] ))
        except:
            print "Duplicate name found"
            # ignore
    mlb.tree.column(artistsHeader[1], anchor='c', width=80)
    mlb.tree.column(artistsHeader[2], anchor='c')
    mlbChanged = False
    lab = Label(row, text="Double click a column\nentry to toggle value", anchor='c', fg='blue')
    lab.pack(side=TOP, padx=15, pady=20)
    apply2Button = ttk.Button(row, text=' Apply ', command=(lambda: apply2(frame)), state=DISABLED)
    apply2Button.pack(side=TOP, padx=15)

    # mlb.tree.bind('<<TreeviewSelect>>', mlbSelectCmd)
    mlb.tree.bind('<Double-Button-1>', mlbDoubleClick)
    # mlb.sortBy(mlb.getColumn())

    # line separator
    row = Frame(frame)
    row.pack(side=TOP, padx=5, pady=10, fill=X)
    separator = Frame(frame, height=5, bd=5, relief=SUNKEN)
    separator.pack(side=TOP, fill=X, padx=5, pady=5)

    # bottom button(s)
    row = Frame(frame)
    b = Button(row, text=' Close ', command=(lambda: onClose(frame)))
    row.pack(side=BOTTOM, fill=X, padx=10, pady=5)
    b.pack(side=RIGHT)

    doUpdates=True
    updateFields()
    top.update()
    th.showHeader(bNameStr.get())
    frame.grab_set()
    top.iconbitmap('myicon.ico')
    frame.mainloop()


def onClosing():
    top.quit()


def admin(frame, eventTracker):
    global top
    global al
    global et

    et = eventTracker
    al = artistlist.ArtistList( et )
    al.readFile(showError=True)
    top = Toplevel()
    top.resizable(0,0)
    #top.grab_set()
    top.focus_force()
    top.wm_title(et.appName() + ": Admin")
    top.protocol("WM_DELETE_WINDOW", onClosing)  # Handle the [x] event
    makeform(top)
    top.destroy()

