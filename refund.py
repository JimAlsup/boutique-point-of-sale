'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

from Tkinter import *
from idlelib.ToolTip import *
from multicolumnlistbox import *
import tkMessageBox
import artistlist
import time
import salefile
import copy
import mail
import printit
import statuspopup

selectedListItem = 0

def mlbSelectCmd(evt):
    global allowRefund
    global lastSelection
    global selectedListItem

    selectedListItem = mlb.getSelectionIndex()  # return the zero based data index
                                                # item iid is + 1
    if lastSelection == selectedListItem:
        # deselect line in pmlb box
        list = pmlb.tree.selection()
        pmlb.tree.selection_remove(list)
        return

    # if previous selection is the same as current then ignore so that the process
    # below does not destroy the state
    allowRefund = cs[selectedListItem].transId.find('R') == -1

    # delete the current set of sale items and repopulate from selected transaction
    for x in range(0,pmlb.sizeIt):
        tagn = "T%d" % (x+1)
        # print tagn + ",",
        pmlb.tree.tag_configure(tagn, foreground='black')
        pmlb.tree.delete(x+1)
    x = 0
    for s in cs[selectedListItem].sales:
        x += 1
        tagn = "T%d" % x
        try:
            ref = s.refunded
            pmlb.tree.insert("", END, x, tag=tagn, values=(s.artist, str(s.multiple)+'X', str62(s.price),
                                                           "", "", "", ref))
        except: # ignore duplicate (Todo: narrow the exception down)
            print "Warning - Exception on pmlb insert - duplicate line found and discarded:", s.artist
    # print "}}}", selectedListItem, lastSelection, id(selectedListItem)
    lastSelection = selectedListItem
    pmlb.sizeIt = x
    r = cs[selectedListItem].paidBy.get()
    checkNumStr.set("")
    refundBy.set(r)
    if r == 2:
        checkNumStr.set(str(cs[selectedListItem].paidBy.checkNum))
    updateButtons()


def pmlbSelectCmd(evt):
    return

# Update the button states, X factor/values of the item fields and
# the total fields
def updateButtons():
    if pmlb.sizeIt == 0 or not allowRefund:
        refund.config(state=DISABLED)
        all.config(state=DISABLED)
        clear.config(state=DISABLED)
        return
    x = 0
    found = False
    none = False
    subTotal = 0.0
    tax = 0.0
    for s in cs[selectedListItem].sales:
        x += 1
        sXF = myint(pmlb.tree.set(x, saleXCol))
        rXF = myint(pmlb.tree.set(x, refundXCol))
        pXF = myint(pmlb.tree.set(x, prevXCol))
        if pXF >= sXF:  # ignore entries already refunded
            tagn = "%dT" % x
            pmlb.tree.tag_configure(tagn, foreground='grey')
            continue
        if rXF > 0:
            pmlb.tree.set(x, refundPCol, (str62(s.price*rXF)))
            pmlb.tree.set(x, refundTCol, (str62(s.tax*rXF)))
            found = True  # at least one marked for refund
        else:
            pmlb.tree.set(x, refundPCol, "")
            pmlb.tree.set(x, refundTCol, "")
            none = True   # at least one not marked for refund
        subTotal += s.price * rXF
        tax += s.tax * rXF
    total = subTotal + tax
    if total <= 0:
        subTotalStr.set("")
        taxStr.set("")
        totalStr.set("")
    else:
        subTotalStr.set(str62(subTotal))
        taxStr.set(str62(tax))
        totalStr.set(str62(total))
    if found:
        refund.config(state=NORMAL)
        clear.config(state=NORMAL)
    else:
        refund.config(state=DISABLED)
        clear.config(state=DISABLED)
    if none:
        all.config(state=NORMAL)
    else:
        all.config(state=DISABLED)

def myint(s):
    if s == "":
        return 0
    i = 0
    while i < len(s) and s[i] in string.digits:
        i += 1
    try:
        v = int(s[0:i])
    except:
        v = 0
    return v


def pmlbDoubleClick(evt):
    # using the selected event location doesn't work right if click is not
    # a selectable item.  Instead it stays on the current resulting in
    # the wrong item being used
    # pmlbSelectedListItem = pmlb.getSelectionIndex()
    id = pmlb.tree.identify_row(evt.y)
    # print "DClk", id
    if id == "" or not allowRefund:
        return
    if refundBy.get() == 2:  # check
        top.bell()
        return
    s = cs[selectedListItem].sales[int(id)-1]
    tagn = "T" + id
    # print tagn
    sXF = myint(pmlb.tree.set(id, saleXCol))
    rXF = myint(pmlb.tree.set(id, refundXCol))
    pXF = myint(pmlb.tree.set(id, prevXCol))
    if pXF >= sXF:  # previously all returned, beep to tell the user
        top.bell()
        return
    if rXF+pXF < sXF:
        rXF += 1
        pmlb.tree.set(id, refundXCol, "%dX" % rXF)
        #pmlb.tree.set(id, refundPCol, (str62(s.price*rXF)))
        #pmlb.tree.set(id, refundTCol, (str62(s.tax*rXF)))
    else:  # cycle back to 0X
        rXF = 0
        pmlb.tree.set(id, refundXCol, "")
        #pmlb.tree.set(id, refundPCol, "")
        #pmlb.tree.set(id, refundTCol, "")
    if rXF > 0:
        pmlb.tree.tag_configure(tagn, foreground='red')
    else:
        pmlb.tree.tag_configure(tagn, foreground='black')
    updateButtons()


def cancel():
    global cancelled
    cancelled = True
    print "cancelled"
    top.quit()


def killTopWindow():
    global cancelled
    cancelled = True
    # print "Killed top window"
    top.quit()
    # no need to destroy the window.  t.quit exits via the mainloop
    # and this has a destroy call

# We want to ignore some keyboard inputs in this dialog
def ignoreKey():
    top.bell()

def str62(f):
    return "$%7.2f" % round(f,2)

def selectAllItems():
    if not allowRefund:
        return
    x = 0
    for s in cs[selectedListItem].sales:
        x += 1
        sXF = myint(pmlb.tree.set(x, saleXCol))
        rXF = myint(pmlb.tree.set(x, refundXCol))
        pXF = myint(pmlb.tree.set(x, prevXCol))
        # print x, s.artist, s.refunded
        if pXF >= sXF:
            continue
        if rXF < sXF-pXF:
           rXF = sXF-pXF
           tagn = "T%d" % x
           pmlb.tree.tag_configure(tagn, foreground='red')
           pmlb.tree.set(x, refundXCol, "%dX" % rXF)
    updateButtons()

def clearAllItems():
    if not allowRefund:
        return
    x = 0
    for s in cs[selectedListItem].sales:
        x += 1
        sXF = myint(pmlb.tree.set(x, saleXCol))
        rXF = myint(pmlb.tree.set(x, refundXCol))
        pXF = myint(pmlb.tree.set(x, prevXCol))
        if pXF >= sXF:
            continue
        if rXF > 0:
           rXF = 0
           tagn = "T%d" % x
           pmlb.tree.tag_configure(tagn, foreground='black')
           pmlb.tree.set(x, refundXCol, "")
    updateButtons()


# Create a sring buffer containing a customer receipt
def createReceipt(transId, date, ci):
    # remove trailing original tid
    i = transId.find('/')
    if i > 0:
        tid = transId[0:i]
    else:
        tid = transId
    hmsg = """\
    <!DOCTYPE html>
    <html>
       <head>
         <meta content="text/html; charset=windows-1252" http-equiv="content-type">
         <title>test</title>
       </head>
    <body>
      <pre style="background-color:#ffffff;color:#000000;font-family:'Courier New';font-size:8.6pt;">"""
    paidName = ["Cash", "Check", "Credit"]
    msg = "Hello %s %-s" % (ci.first, ci.last)
    hmsg += msg
    while len(msg) < 47:
        msg += " "
        hmsg += " "
    msg += "Date: %s\n%47sTransaction Id: %s\nRefund receipt:" % (date, "", tid)
    hmsg += "Date: %s<br>%47sTransaction Id: %s<br>Refund receipt:" % (date, "", tid)

    paidName = ["Cash", "Check", "Credit"]
    # msg = et.getBoutiqueName() + " - " + et.getEventName() + "\n"
    # msg += "Refund Receipt:%44s %s\n%62s %s\n" % ("Date:", date, "Refund Transaction Id:", transId)
    msg += "%47s %s\n" % ("Orig Sale Transaction Id:", ci.transId)
    hmsg  += "%47s %s<br>" % ("Orig Sale Transaction Id:", ci.transId)
    # msg += "Hello %s %-s,\n\n  Items Refunded:\n" % (ci.first, ci.last)

    totalPrice = 0.0
    totalTax = 0.0
    id = 0
    msg += "\n    %-30s%15s  %8s  %8s\n" % ("Artist/Vendor", "Multiple", " Unit/P", "Total/P")
    msg += "    %-30s%15s  %8s  %8s\n" % ("-----------------------------", "--------", "--------", "--------")
    hmsg += "<br>    %-30s%15s  %8s  %8s<br>" % ("Artist/Vendor", "Multiple", " Unit/P", "Total/P")
    hmsg += "    %-30s%15s  %8s  %8s<br>" % ("-----------------------------", "--------", "--------", "--------")
    for s in ci.sales:
        id += 1
        # print "[", s, ']'
        rXF = myint(pmlb.tree.set(id, refundXCol))
        if rXF == 0:
            continue
        msg += "    %-30s%11s %2dX  $%7.2f  $%7.2f\n" % (s.artist, "Refund", rXF, s.price, s.price*rXF)
        hmsg += "    %-30s%11s %2dX  $%7.2f  $%7.2f<br>" % (s.artist, "Refund", rXF, s.price, s.price*rXF)
        totalPrice += s.price * rXF
        totalTax += s.tax * rXF
    if totalPrice == 0:
        top.bell()
        return
    total = totalPrice + totalTax
    msg += "%69s\n" % "--------"
    msg += "%60s %8s\n" % ("Sub Total:", str62(totalPrice))
    msg += "%60s %8s\n" % ("Tax:", str62(totalTax))
    msg += "%60s %8s\n" % ("Total Refund:", str62(total))
    hmsg += "%69s<br>" % "--------"
    hmsg += "%60s %8s<br>" % ("Sub Total:", str62(totalPrice))
    hmsg += "%60s %8s<br>" % ("Tax:", str62(totalTax))
    hmsg += "%60s %8s<br>" % ("Total Refund:", str62(total))
    r = refundBy.get()
    if r == 1:
        msg += "\n%60s %8s\n" % ("Cash Returned:", str62(total))
        hmsg += "<br>%60s %8s<br>" % ("Cash Returned:", str62(total))
    elif r == 2:
        msg += "\n%60s %8s Returned\n" % ("Check #" + str(ci.paidBy.checkNum) + " For:", str62(total))
        hmsg += "<br>%60s %8s<br>" % ("Cash Returned:", str62(total))
    else:
        msg += "\n%60s %8s\n" % ("Credit Card Credited For:", str62(total))
        hmsg += "<br>%60s %8s<br>" % ("Cash Returned:", str62(total))

    msg += "\nThank you for visiting,\n\n\t" + et.getBoutiqueName() + " - " + et.getEventName() + "\n"
    hmsg += "<br>Thank you for visiting,<br><br>\t" + et.getBoutiqueName() + " - " + et.getEventName() + "<br>"
    return msg, hmsg


def refundUpdate():
    # print refundBy.get()
    return

def refundCallback(status, count):
    status.addStatus("Recording Refund Record...")
    ci = cs[selectedListItem]
    # test email parameters
    emailStr = ci.email
    emailStr.strip()
    date = time.strftime("%m-%d-%Y %H:%M")

    rcs = copy.copy(ci)
    rcs.date = date
    rcs.sales = []
    subTotal = 0
    taxTotal = 0
    id = 0
    for s in ci.sales:
        id += 1
        rXF = myint(pmlb.tree.set(id, refundXCol))
        if rXF == 0:
            continue
        print "[", s, "]"
        rcs.addSale(s.artist, rXF, s.price, s.tax)
    sf = salefile.SaleFile(et)
    worked, err = sf.record(rcs, False, rcs.transId)
    if not worked:
        status.addStatus(err,'red')
        return 0
    status.addStatus("New refund transaction created: " + sf.transId)
    receiptStr, receiptHTMLStr = createReceipt(sf.transId, date, ci)
    et.newReceipt(receiptStr)

    # '''
    if emailReceiptInt.get() == 1 and len(emailStr) > 0:
        status.addStatus("Emailing Receipt...")
        result = mail.sendHTMLEMail(et.fromAddr, et.login, et.smtp, et.password, emailStr,
                                et.getBoutiqueName() + " - Refund Receipt", receiptStr, receiptHTMLStr)
        if result != "":
            status.addStatus(result, 'red')
    # '''

    if printReceiptInt.get() == 1:
        status.addStatus("Printing Receipt...")
        result = printit.printString(receiptStr)
        if result != "":
            status.addStatus(result, 'red')
    return 0

def refundItems():
    ci = cs[selectedListItem]
    # test email parameters
    emailStr = ci.email
    emailStr.strip()
    el = len(emailStr)
    if emailReceiptInt.get() == 1:
        if el == 0:
         tkMessageBox.showwarning(
            "Error",
            "Email receipt option selected, but email address field is blank",
            parent=top
         )
         return False
        if emailStr.find('@') == -1 or emailStr.find(' ') != -1:
            tkMessageBox.showwarning(
                "Error",
                "Email address (%s) must have @ and no spaces\nPlease fix and retry" % emailStr,
                parent=top
            )
            return False

    print "Starting StatusPop..."
    x = refund.winfo_rootx()+20
    y = refund.winfo_rooty()-50
    status = statuspopup.StatusPopUp(top, "Sale", refundCallback, x, y)
    print "Exiting StatusPop"

    top.quit()
    return True


# Create the New Sale UI form
def makeform(frame):
    global mlb
    global pmlb
    global refundXCol, prevXCol, saleXCol
    global refundPCol, refundTCol, refund
    global all, clear  # Button objects
    global subTotalStr, taxStr, totalStr
    global checkNumStr, refundBy
    global emailReceiptInt, printReceiptInt
    global toplab

    refundXCol = "R X"
    prevXCol = "Prev R X"
    saleXCol = "X "
    refundPCol = "R Price"
    refundTCol = "R Tax"

    courier = tkFont.Font(family='Courier', size=11, weight='normal')
    script = tkFont.Font(family='Adine Kirnberg', size=26, weight='bold')
    #script = tkFont.Font(family='Brush Script MT', size=18, weight='normal')

    bName = et.getBoutiqueName() + " - " + et.getEventName()
    th = utils.TitleDisplay(frame)

    # Sale Items
    row=Frame(frame)
    row.pack(side=TOP, fill=X, padx=5, pady=5)
    lab = Label(row, text="Sales List:   Select a transaction to view or refund.", anchor='w')
    lab.pack(side=LEFT)

    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=5)
    lookupHeader = ['Trans',
                    'First',
                    'Last',
                    'email',
                    'Date',
                    'Paid by']
    mlb = MultiColumnListbox(row, lookupHeader, 7, returnFunc=ignoreKey)
    i = 1
    if cs != None:
        for list in cs:
            #print list.transId, list.paidBy.text, list.paidBy.checkNum
            try:
                mlb.tree.insert("", END, i, values=(list.transId, list.first, list.last, list.email,
                                                 list.date, list.paidBy.text))
            except:
                print "Warning - Duplicate transaction id found and discarded"
                # ignore duplicate
            i += 1
    # mlb.tree.config(width=40)
    mlb.tree.column(lookupHeader[5], anchor='w', width=90)
    mlb.tree.column(lookupHeader[4], anchor='w', width=110)
    mlb.tree.column(lookupHeader[3], anchor='w', width=210)
    mlb.tree.column(lookupHeader[2], anchor='w', width=120)
    mlb.tree.column(lookupHeader[1], anchor='w', width=120)
    mlb.tree.column(lookupHeader[0], anchor='w', width=90)
    mlb.tree.bind('<<TreeviewSelect>>', mlbSelectCmd )
    mlb.sortBy(lookupHeader[0])
    tip = ToolTip(mlb.tree, "Select a sales transaction to view or refund below")
    # print frame.winfo_width(), mlb.tree.winfo_width()

    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=5, expand=Y)
    purchaseHeader = ['Artist/Vendor      ', saleXCol,'U Price', refundXCol, refundPCol,
                      refundTCol, prevXCol]
    pmlb = MultiColumnListbox(row, purchaseHeader, 7, returnFunc=ignoreKey)
    pmlb.tree.column(purchaseHeader[6], anchor='c', width=55)
    pmlb.tree.column(purchaseHeader[5], anchor='e', width=45)
    pmlb.tree.column(purchaseHeader[4], anchor='e', width=56)
    pmlb.tree.column(purchaseHeader[3], anchor='c', width=50)
    pmlb.tree.column(purchaseHeader[2], anchor='e', width=56)
    pmlb.tree.bind('<<TreeviewSelect>>', pmlbSelectCmd)
    pmlb.tree.bind('<Double-Button-1>', pmlbDoubleClick)
    tip = ToolTip(pmlb.tree, "Sale items in this transaction")
    #lab = Label(row, text="* - already refunded", anchor=W)
    #lab.pack(side=TOP, padx=5,pady=5)
    lab = Label(row, text="Double click item line to refund\nClick multiple times for X factor.",
                anchor=W, fg="blue")
    lab.pack(side=TOP, padx=0,pady=3)
    lab = Label(row, text="X - in \"" + refundXCol + "\" column indicates\n item(s) to be refunded.",
                anchor=W, fg="blue")
    lab.pack(side=TOP, padx=0,pady=5)
    all = ttk.Button(row, text=' Select All Items for Refund ', command=selectAllItems)
    all.pack(side=TOP, padx=0,pady=5)
    clear = ttk.Button(row, text='  Clear All Items for Refund  ', command=clearAllItems)
    clear.pack(side=TOP, padx=0,pady=5)


    # subtotal
    row = Frame(frame)
    row.pack(side=TOP, padx=5,pady=0, fill=X)
    lab = Label(row, width=15, text="Sub Total:", anchor='e')
    subTotalStr = StringVar()
    subTotal = Label(row, width=8, text="$0.00", borderwidth=1, relief=GROOVE, bg="white", anchor="e",
                     textvariable=subTotalStr)
    lab.pack(side=LEFT, padx=3)
    subTotal.pack(side=LEFT, padx=2)

    # tax
    taxLabelStr = "Tax %3.2f%%:" % (et.taxRate * 100)
    lab = Label(row, text=taxLabelStr, anchor='e')
    lab.pack(side=LEFT, padx=3)
    taxStr = StringVar()
    tax = Label(row, width=8, text="$0.00", borderwidth=1, relief=GROOVE, bg="white", anchor="e",
                textvariable=taxStr)
    tax.pack(side=LEFT, padx=2)

    # totals row
    lab = Label(row, text="Refund Total:", anchor='e', )
    totalStr = StringVar()
    total = Label(row, width=8, text="$0.00", borderwidth=1, relief=GROOVE, bg="white", anchor="e",
                  textvariable=totalStr)
    lab.pack(side=LEFT, padx=3)
    total.pack(side=LEFT, padx=0)
    # total.place(x=340, y=1)

    separator = Frame(frame, height=5, bd=5, relief=SUNKEN)
    separator.pack(side=TOP, fill=X, padx=5, pady=10)

    row = Frame(frame)
    row.pack(side=TOP, padx=5,pady=5, fill=X)
    lab = Label(row, width=10, text="Refunded by:", anchor='w')
    lab.pack(side=LEFT,padx=10)
    refundBy = IntVar()
    refundBy.set(1)
    caRButton = ttk.Radiobutton(row, text="Cash", variable=refundBy, value=1, state=DISABLED,
                                command=(lambda: refundUpdate()))
    caRButton.pack(side=LEFT, anchor='w')
    ckRButton = ttk.Radiobutton(row, text="Returned Check #", variable=refundBy, value=2,
                                state=DISABLED, command=(lambda: refundUpdate()))
    ckRButton.pack(side=LEFT, anchor='w')
    checkNumStr = StringVar()  # textvariable is needed to allow callback for changes
    checkNumStr.trace("w", lambda name, index, sv=checkNumStr: updateButtons())
    checkNum = Entry(row, width=8, state=DISABLED, textvariable=checkNumStr)
    checkNum.insert(0, "")
    checkNum.pack(side=LEFT)
    ccRButton = ttk.Radiobutton(row, text="Credit\nCard", variable=refundBy, value=3,
                                state=DISABLED, command=(lambda: refundUpdate()))
    ccRButton.pack(side=LEFT, padx=5, anchor='w')
    emailReceiptInt = IntVar()
    emailReceipt = Checkbutton(row, text="Email\nReceipt", variable=emailReceiptInt)
    emailReceipt.select()
    emailReceipt.pack(side=LEFT, padx=20)
    if et.allowPrintReceipt:
        state = NORMAL
    else:
        state = DISABLED
    printReceiptInt = IntVar()
    printReceipt = Checkbutton(row, text="Print\nReceipt", state=state, variable=printReceiptInt)
    printReceipt.pack(side=LEFT, padx=5)

    refund = ttk.Button(row, text='  Refund Items  ', command=refundItems)
    refund.pack(side=RIGHT, padx=5,pady=3)
    tip = ToolTip(refund, "Refund the selected items")

    row = Frame(frame)
    row.pack(side=TOP, padx=5,pady=10, fill=X)
    separator = Frame(frame, height=5, bd=5, relief=SUNKEN)
    separator.pack(side=TOP, fill=X, padx=5, pady=5)

    # bottom buttons (close and clear form)
    row = Frame(frame)
    #b2 = Button(row, text='Clear Form', command=lambda: clearForm())
    b3 = ttk.Button(row, text='  Cancel / Close  ', command=frame.quit)
    row.pack(side=BOTTOM, fill=X, padx=10, pady=5)
    #b2.pack(side=LEFT, padx=5)
    b3.pack(side=RIGHT)

    top.update()
    updateButtons()
    th.showHeader(bName)
    mlb.focusSelectTop()
    top.iconbitmap('myicon.ico')
    frame.mainloop()

def onClosing():
    top.quit()


def refundSale(eventtrack):
    global top
    global al
    global et
    global cs
    global allowRefund
    global lastSelection

    et = eventtrack
    allowRefund = False
    lastSelection = -1

    # read in the artist data
    al = artistlist.ArtistList(et)
    sf = salefile.SaleFile(et)
    #sf.debug = True
    cs = sf.readAllSales()

    if not al.readFile:
        print "Unable to load artist list file"
        return
    top = Toplevel()
    top.resizable(0,0)
    #top.grab_set()
    top.focus_force()
    top.wm_title(et.appName() + ": Refund Sale")
    top.protocol("WM_DELETE_WINDOW", onClosing)  # Handle the [x] event
    makeform(top)
    top.destroy()

