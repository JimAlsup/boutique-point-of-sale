# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
    Boutique Point of Sale is a simple point of sale application designed for use
    by a collection of artisans holding a boutique sale.  It allows for multiple points
    of sale stations (PCs). Artisan specific sales are tracked and refund options are allowed
    with both printed and email receipts.  Event reports can be generated for each artisan allowing
    for the easy tracking/distribution of proceeds.  Taxes are also configurable per artisan. 
* Version 0.0.1

### How do I get set up? ###

* Summary of set up
    For Users
        Client install is located in Output\bpos_setup.exe
    For builders/contributors
        Built using Python 2.7
        PyCharm community edition is the IDEA used.
        Install the python package portalocker (file locking)
        Install the python package openpyxl
        Install cx_freeze for packaging app into a self contained executable. See 
        makepos.bat for build script
        Install Inno setup for creating single setup program 
* Configuration
    At first startup use the database directory browse button to locate an existing
    database or create a new one.  If multiple sale terminals are needed choose a
    shared location.
* Dependencies
    For execution Windows is the only dependency.  Building requires steps listed in
    Setup.
* Database configuration
    The database consists of a collection of Excel files (.xlsx) meant also for 
    consumption by excel/humans. The database location 
* How to run tests
    No tests at the moment - just usage testing
* Deployment instructions
    The executable Output/bpos_setup.exe file is a self contained singled file setup
    build using Inno setup.
    See the files makesetup.bat and BoutiquePOS_Setup.iss for details.  

### Contribution guidelines ###
    At the moment this project is alpha and closed to outside additions.  Message the authors
    if you have comments, wish to contribute, or have usage questions.  

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
    Meia Alsup, Jim Alsup, Judy Chen  