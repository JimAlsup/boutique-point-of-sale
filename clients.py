'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

from openpyxl import Workbook
from openpyxl.worksheet import *
from openpyxl import load_workbook

import tkMessageBox
import copy


'''
Insert logic borrowed from here:

https://bitbucket.org/snippets/openpyxl/qyzKn

Note that over time openpyxl may change such that this code needs updating.  Two choices:
Either update this code or install/reference the compatible version of openpyxl.  Version
used to create this code is 2.3.3.  http://openpyxl.readthedocs.org/en/2.3.3/
'''

def loadCustomerData(et):
    wb = load_workbook(filename = et.customerDatabaseFileName(), read_only=True)
    wb._archive.fp.close()  # work around bug in load_workbook.
                            # Archive file is not properly closed - close it
    ws = wb['Sheet1']
    list = []

    # match all last names, first and emails and build a list
    index = 1
    for rowX in ws.rows:
        rXa = rowX[0].value
        if rXa != None and rXa.strip()[0] == '#':
            continue  # skip comment lines
        rXb = rowX[1].value
        rXc = rowX[2].value
        if rowX[0].data_type != 's':
            rXa= ""
        if rowX[1].data_type != 's':
            rXb= ""
        if rowX[2].data_type != 's':
            rXc= ""
        list.append((rXb, rXa, rXc))  # we need fist, last, email in the tuple
        index += 1
    return list



def insert_rows(self, row_idx, cnt, above=False, copy_style=True, fill_formulae=True):
    """Inserts new (empty) rows into worksheet at specified row index.

    :param row_idx: Row index specifying where to insert new rows.
    :param cnt: Number of rows to insert.
    :param above: Set True to insert rows above specified row index.
    :param copy_style: Set True if new rows should copy style of immediately above row.
    :param fill_formulae: Set True if new rows should take on formula from immediately above row, filled with references new to rows.

    Usage:

    * insert_rows(2, 10, above=True, copy_style=False)

    """
    CELL_RE  = re.compile("(?P<col>\$?[A-Z]+)(?P<row>\$?\d+)")

    row_idx = row_idx - 1 if above else row_idx

    def replace(m):
        row = m.group('row')
        prefix = "$" if row.find("$") != -1 else ""
        row = int(row.replace("$",""))
        row += cnt if row > row_idx else 0
        return m.group('col') + prefix + str(row)

    # First, we shift all cells down cnt rows...
    old_cells = set()
    old_fas   = set()
    new_cells = dict()
    new_fas   = dict()
    for c in self._cells.values():

        old_coor = c.coordinate

        # Shift all references to anything below row_idx
        if c.data_type == Cell.TYPE_FORMULA:
            c.value = CELL_RE.sub(
                replace,
                c.value
            )
            # Here, we need to properly update the formula references to reflect new row indices
            if old_coor in self.formula_attributes and 'ref' in self.formula_attributes[old_coor]:
                self.formula_attributes[old_coor]['ref'] = CELL_RE.sub(
                    replace,
                    self.formula_attributes[old_coor]['ref']
                )

        # Do the magic to set up our actual shift
        if c.row > row_idx:
            old_coor = c.coordinate
            old_cells.add((c.row,c.col_idx))
            c.row += cnt
            new_cells[(c.row,c.col_idx)] = c
            if old_coor in self.formula_attributes:
                old_fas.add(old_coor)
                fa = self.formula_attributes[old_coor].copy()
                new_fas[c.coordinate] = fa

    for coor in old_cells:
        del self._cells[coor]
    self._cells.update(new_cells)

    for fa in old_fas:
        del self.formula_attributes[fa]
    self.formula_attributes.update(new_fas)

    # Next, we need to shift all the Row Dimensions below our new rows down by cnt...
    for row in range(len(self.row_dimensions)-1+cnt,row_idx+cnt,-1):
        new_rd = copy.copy(self.row_dimensions[row-cnt])
        new_rd.index = row
        self.row_dimensions[row] = new_rd
        del self.row_dimensions[row-cnt]

    # Now, create our new rows, with all the pretty cells
    row_idx += 1
    for row in range(row_idx,row_idx+cnt):
        # Create a Row Dimension for our new row
        new_rd = copy.copy(self.row_dimensions[row-1])
        new_rd.index = row
        self.row_dimensions[row] = new_rd
        for col in range(1,self.max_column):
            col = get_column_letter(col)
            cell = self.cell('%s%d'%(col,row))
            cell.value = None
            source = self.cell('%s%d'%(col,row-1))
            if copy_style:
                cell.number_format = source.number_format
                cell.font      = source.font.copy()
                cell.alignment = source.alignment.copy()
                cell.border    = source.border.copy()
                cell.fill      = source.fill.copy()
            if fill_formulae and source.data_type == Cell.TYPE_FORMULA:
                s_coor = source.coordinate
                if s_coor in self.formula_attributes and 'ref' not in self.formula_attributes[s_coor]:
                    fa = self.formula_attributes[s_coor].copy()
                    self.formula_attributes[cell.coordinate] = fa
                # print("Copying formula from cell %s%d to %s%d"%(col,row-1,col,row))
                cell.value = re.sub(
                    "(\$?[A-Z]{1,3}\$?)%d"%(row - 1),
                    lambda m: m.group(1) + str(row),
                    source.value
                )
                cell.data_type = Cell.TYPE_FORMULA

    # Check for Merged Cell Ranges that need to be expanded to contain new cells
    for cr_idx, cr in enumerate(self.merged_cell_ranges):
        self.merged_cell_ranges[cr_idx] = CELL_RE.sub(
            replace,
            cr
        )

# end borrowed code

# Tests for name matches:
#   returns 0 for no matches
#   returns 1 for first argument match only
#   return  2 for email match only
#   returns 3 for both first and email matches
def matchEither( row, first, email):
    rfirst = row[1].value
    remail = row[2].value
    if rfirst == None:
        rfirst = ""
    if remail == None:
        remail = ""
    m1 = rfirst.upper() == first
    m2 = remail.lower() == email
    if m1:
        if m2:
            return 3
        return 1
    if m2:
        return 2
    return 0



def addCustomerRecord(et, last, first, email):
    wb = load_workbook(filename = et.customerDatabaseFileName())
    ws = wb['Sheet1']

    last = last.strip().upper()
    first = first.title()
    ufirst = first.strip().upper()
    email = email.strip().lower()

    # match all last names, first and emails and build a list
    index = 0
    directMatchLast = False
    match = 0
    lindex = 0
    for row in ws.rows:
        index += 1
        rlast = row[0].value
        if rlast == None:
            rlast = ""
        rlast = rlast.upper()
        if directMatchLast:
            if rlast.find('#') == 0:
                continue
            if rlast != last:
                # print "match last name, but not other matches"
                break  # we're done here
            match = matchEither(row, ufirst, email)
            # print index, match
            if match >= 2:  # exact match or email
                lindex = index
                # print "matched more2", match
                break
            if match == 1:
                lindex = index  # record, but keep going looking for better
            continue
        if rlast == None:
            continue
        if rlast.find('#') == 0:
            continue
        if rlast == last:
            # now we need to check ufirst name and or email for match
            # print "last name match"
            directMatchLast = True
            match = matchEither(row, ufirst, email)
            lindex = index  # best match so far
            if match >= 2:  # exact match, or email match
                # print "matched more", match
                break
        elif last < rlast:
            if last == "":  # special case as sorting of '' is at the end
                continue
            lindex = index
            # print last, rlast
            break   # no match, were done with this loop

    # lindex equal to overwrite or insertion point
    if match < 2:
        # print "inserting"
        Worksheet.insert_rows = insert_rows
        ws.insert_rows(lindex, 1, above=True, copy_style=True)
    if match < 3:  # update all the customer record fields
        ws.cell(row=lindex, column=1).value = last.upper()
        ws.cell(row=lindex, column=2).value = first
        ws.cell(row=lindex, column=3).value = email
    ws.cell(row=lindex, column=4).value = "x"
    alignment = ws.cell(row=lindex,column=4).alignment.copy(horizontal='center')
    ws.cell(row=lindex, column=4).alignment = alignment
    wb.save(filename=et.customerDatabaseFileName())
    # print last, first, email, "at", lindex


# inserts one column at pos col (integer
def insertEventInWorksheet(et, top, filename, column_index, name):
    # copy the column width adding in the new column with oolumn width taken
    # from the column being inserted at
    # copy the cell data while inserting new cell
    r = 0

    try:
        wb = load_workbook(filename=filename, read_only=True)
        wb._archive.fp.close()  # work around bug in load_workbook.
                                # Archive file is not properly closed - close it
    except IOError:
        tkMessageBox.showerror("Error", "Missing %s file.\nCopy the template from the program dir."
                               % et.getClientFileName(), parent=top)
        return
    ws = wb.worksheets[0]

    wbc = Workbook()
    wsc = wbc.active
    wsc.title = "Sheet1"

    for row in ws.rows:
        r += 1
        adjust = 0
        # print "row", r, ws.max_column,
        c = 0
        for col in row:
            c += 1
            if c == column_index:
                cell = wsc.cell(row=r,column=c)
                if r == 1:
                    cell.value = name
                else:
                    cell.value = ""
                if col.value != None:
                    cell.alignment = col.alignment.copy()
                    cell.font      = col.font.copy()
                    cell.border    = col.border.copy()
                    cell.fill      = col.fill.copy()
                adjust += 1
            # column_letter = get_column_letter(c)
            cell = wsc.cell(row=r,column=c + adjust)
            cell.value = col.value
            if cell.value != None:
                cell.alignment = col.alignment.copy()
                cell.font      = col.font.copy()
                cell.border    = col.border.copy()
                cell.fill      = col.fill.copy()
                cell.number_format = col.number_format
        # print ""
    # we cannot copy widths of cells, so just go through the list and set them
    widths = [22,13,30,13,0]
    for i in range(len(widths)):
        letter = get_column_letter(i+1)
        wsc.column_dimensions[letter].width = widths[i]

    wbc.save(filename)




'''
# Test code
Worksheet.insert_rows = insert_rows

wb = load_workbook(filename = 'test.xlsx')
ws = wb.worksheets[0]

ws.insert_rows(2, 2, above=True, copy_style=False)
ws.cell(row=2, column=3).value = "Well?"

wb.save("test.xlsx")
------------------------------------------------------
insertEventInWorksheet("clientes.xlsx", 4, "Spring \n2016")
'''
