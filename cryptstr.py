'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

# Simple string encryption to keep prying eyes safe

baseStr = "%$GDr6%85498%xzm@1+?/,`~5ye!)97g=;:"

def encryptStr(password, base=None):
    if base is None:
        base = baseStr
    if type(password) != str:
        return None   # Error
    e = ""
    for i in range(len(password)):
        e += "%02x" % (ord(password[i]) ^ ord(base[i]))
    return e


def decryptStr(password, base=None):
    if base is None:
        base = baseStr
    p = ""
    for i in range(0,len(password),2):
        p += chr(int(password[i:i+2],16) ^ ord(base[i/2]))
    return p


if __name__ == "__main__":
    print "Testing"
    testpw = "this_my_password"
    test_pw_crypted = encryptStr(testpw)
    print  testpw, test_pw_crypted, decryptStr(test_pw_crypted)


