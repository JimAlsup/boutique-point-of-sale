'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

# mail.py: sends an email

import smtplib
import string
import time
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


# aroundtheworldusa@gmail.com
# ATWB@2003


def sendEMail( fromAddr, login, smtp, password, to, subject, content ):
    body = string.join((
            "From: %s" % fromAddr,
            "To: %s" % to,
            "Subject: %s" % subject,
            "",
            content
            ), "\r\n")

    # Send the message via local SMTP server.
    # print body
    s = smtplib.SMTP(smtp)
    s.starttls()
    s.login(login, password)
    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.
    try:
        s.sendmail(fromAddr, to, body)
    except:
        return "Error sending email - is address correct?"
    s.quit()
    return ""  # signal success

def sendHTMLEMail( fromAddr, login, smtp, password, to, subject, content, html ):
    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = fromAddr
    msg['To'] = to

    part1 = MIMEText(content, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via local SMTP server.
    s = smtplib.SMTP(smtp)
    s.starttls()
    s.login(login, password)
    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.
    try:
        s.sendmail(fromAddr, to, msg.as_string())
    except:
        return "Error sending email - is address correct?"
    s.quit()
    return ""  # signal success



if __name__ == "__main__":
    msg = string.join((
        "Hello Customer \r\n\r\n"
        "  Below is your purchase receipt on %s:\r\n\r\n" % time.strftime("%m-%d-%Y at %H:%M"),
        "Thank you for your purchase.",
        ), "\r\n")
    sendEMail("from", "login", "smtp" "password", "jim.alsup@gmail.com", "Test", msg)