'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

from Tkinter import *
from idlelib.ToolTip import *
import tkMessageBox
import tkFont
import ttk
import artistlist
import mail
import time
import lookupcust
import editsaleitem
import custsale
import salefile
import clients
import utils
import searchbox
import printit
import statuspopup

skipUpdateButtons = True

# Parses window geometry string as return from widgit.winfo_geometry()
# XlenxYlen+locX+locY  We just want loc and width of x
# return width x location and widget width
def locX(geoStr):
    xloc = -1
    len = -1
    x = geoStr.find('x')
    if x >= 0:
        len = int(geoStr[0:x])
    plus = geoStr.find('+')
    if plus >= 0:
        geoStr = geoStr[plus+1:]
    plus = geoStr.find('+')
    if plus >= 0:
        xloc = int(geoStr[:plus])
    return xloc, len


# update button states based on current sales form data
def updateButtons():
    # print "Update Buttons ", cost.get()
    # This function gets call during construction time when some gui items are not setup.
    # avoid this with this boolean
    if skipUpdateButtons:
        return
    last = custLastName.get().strip()
    em = email.get().strip()
    first = custFirstName.get().strip()
    tender = tenderedStr.get().strip()
    if paidBy.get() != custsale.PaidBy.check:
        checkNum.config(state=DISABLED)
    else:
        checkNum.config(state=NORMAL)
    if paidBy.get() == custsale.PaidBy.cash:
        tendered.config(state=NORMAL)
        returned.config(bg='white')
    else:
        tendered.config(state=DISABLED)
        returned.config(bg='white smoke')
    if itemList.size() == 0 or (last == "" and first == "" and em == ""):
        completeSaleB.config(state=DISABLED)
    else:
        completeSaleB.config(state=NORMAL)
    # curselection return a tuple (empty if none)
    if len(itemList.curselection()) == 0:
        deleteButton.config(state=DISABLED)
        editButton.config(state=DISABLED)
    else:
        deleteButton.config(state=NORMAL)
        editButton.config(state=NORMAL)
    err = False
    value = 0
    try:
        value = float(price.get())
    except:
        err = True
    if err or value == 0 or artist.current() == -1:
        addButton.config(state=DISABLED)
    else:
        addButton.config(state=NORMAL)



 # deletes the selected sales item
def deleteItemButton():
    csl = itemList.curselection()
    for i in csl:
        itemList.delete(i)
        # does this even work
        del valList[i]
        del valListMult[i]
        del valListArtistIndex[i]
        del valListTax[i]
    calcUpdate()
    updateButtons()


# deletes the selected sales item
def editItemButton():
    csl = itemList.curselection()
    if len(csl) > 1:
        print "Shouldn't happen! editItemButton with no selection"
        return
    i = csl[0]
    # print "sel ", i
    aindex, xfactor, value = editsaleitem.edit(t, artistList, valListArtistIndex[i], valListMult[i], valList[i])
    if aindex == 0 and xfactor == 0 and value == 0:
        # print "Edit canceled"
        return  # cancelled
    # print i, aindex, xfactor, value
    valList[i] = value
    valListMult[i] = xfactor
    valListArtistIndex[i] = aindex # the index into artistList
    line, taxes = makeItemLine(artistList[aindex], xfactor, value, taxable[aindex])
    valListTax[i] = taxes
    itemList.delete(i)
    itemList.insert(i, line)
    # print "insert ", i, aindex, line
    calcUpdate()
    updateButtons()


# revert just the artist/cost data fields to defaults
def clearFields():
    # artist.current(0)
    artist.set("")
    price.delete(0, END)
    price.insert(0, ".00")
    multi.current(0)
    # itemDesc.delete(0, END)


# revert sales form to default values
def clearForm():
    itemList.delete(0, END)
    del valList[:]
    del valListArtistIndex[:]
    del valListTax[:]
    del valListMult[:]
    clearFields()
    email.delete(0, END)
    custLastName.delete(0, END)
    custFirstName.delete(0,END)
    paidBy.set(custsale.PaidBy.cash)
    returnedStr.set("")
    find.current(0)
    search.sbStr.set("")
    calcUpdate()


# recalculate and update various fields in the sales form
def calcUpdate():
    subTotalX = 0
    taxX = 0
    for i in range(0,len(valList)):
        subTotalX += valList[i] * valListMult[i]
        taxX += valListTax[i] * valListMult[i]
    if subTotalX == 0:
        subTotalStr.set("")
        taxStr.set("")
        totalStr.set("")
        tenderedStr.set(".00")
        return
    totalX = subTotalX + taxX
    subTotalStr.set("$%7.2f" % subTotalX)
    taxStr.set("$%7.2f" % taxX)
    totalStr.set("$%7.2f" % totalX)
    err = False
    try:
        f = float(tenderedStr.get())
    except:
        err = True
    if not err and f != 0:
        if paidBy.get() != custsale.PaidBy.cash:
            returnedStr.set("")
        else:
            returnedStr.set("$%7.2f" % (f - totalX))
    else:
         returnedStr.set("")


def doUpdates():
    calcUpdate()
    updateButtons()


def onComboSelect(evt):
    updateButtons()


# called when the a line in the item list is selected
def onItemSelect(evt):
    # Note here that Tkinter passes an event object to onselect(), but we ignore it
    # because we are called elsewhere without an evt
    # w = evt.widget
    # index = int(w.curselection()[0])
    csl = itemList.curselection()
    updateButtons()
    # csl = itemList.curselection()


# Scan the excel customer data base looking for matches to
# all three (last, first name, and email)
def lookupButtonCall():
    lastname, firstname, emailaddr = lookupcust.lookupCustomer(et, top, custLastName.get(),
                                                               custFirstName.get(), email.get())
    ## print ">> ", lastname, firstname, emailaddr
    if (lastname == "" and firstname == "" and emailaddr == "" ):
        # no match
        top.focus_force()
        return
    # Update customer information
    custLastName.delete(0,END)
    custLastName.insert(0,lastname)
    custFirstName.delete(0,END)
    custFirstName.insert(0, firstname)
    email.delete(0,END)
    email.insert(0, emailaddr)
    top.focus_force()


def makeItemLine(name, xfactor, value, taxable):
    temp = "%-32s %2dX $%6.2f  " % (name + ',', xfactor, value)
    taxes = 0
    if taxable:
        taxes = round(value * et.taxRate, 2)
        temp += "%4.2f" % taxes
    # itemList.insert(END, "%-35s  $%6.2f  %4.2f" % (artist.get()[:35] + ',', value, taxes))
    return temp, taxes


# adds a new sales item based on the artist and cost fields
def addSaleItem():
    costStr = price.get()
    err = False
    value = 0
    try:
        value = float(costStr)
    except:
        err = True
    if err or value == 0:
        tkMessageBox.showwarning(
            "Error",
            "Cost value %s is invalid" % costStr,
            parent=top
        )
        return

    # create display string and limit sizes of artist and item description
    '''
    d = itemDesc.get()[:15]
    if len(d) > 0:
        d += ','
    itemList.insert(END, "%-26s %-16s $%6.2f x" % (artist.get()[:25] + ',', d, value),taxStr)
    '''
    taxem = taxable[artist.current()]
    temp, taxes = makeItemLine( artist.get(), int(multi.get()), value, taxem )
    # print temp, taxes, artist.current(), artist.get()
    itemList.insert(END, temp)
    valList.append(value)
    valListTax.append(taxes)
    valListMult.append(int(multi.get()))
    # get the index value from the combobox
    valListArtistIndex.append(artist.current())
    # print ">> artist.current:", artist.current(), artistList[artist.current()]
    clearFields()
    updateButtons()
    calcUpdate()


# Create a sring buffer containing a customer receipt
def createReceipt(transId, date):
    hmsg = """\
    <!DOCTYPE html>
    <html>
       <head>
         <meta content="text/html; charset=windows-1252" http-equiv="content-type">
         <title>test</title>
       </head>
    <body>
      <pre style="background-color:#ffffff;color:#000000;font-family:'Courier New';font-size:8.6pt;">"""
    paidName = ["Cash", "Check", "Credit"]
    msg = "Hello %s %-s" % (custFirstName.get(), custLastName.get())
    hmsg += msg
    while len(msg) < 40:
        msg += " "
        hmsg += " "
    msg += "Date: %s\n%40sTransaction Id: %s\nSales receipt:\n" % (date, "", transId)
    hmsg += "Date: %s<br>%40sTransaction Id: %s<br>Sales receipt:<br>" % (date, "", transId)
    for i in range(0, itemList.size()):
        # print "item = ", itemList.get(i)
        t = "    %-40s  %2dX  $%7.2f" % (artistList[valListArtistIndex[i]], valListMult[i], valList[i])
        msg += t + '\n'
        hmsg += t + "<br>"
        # old way: msg += "     " + itemList.get(i) + "\n"
    msg += "%59s\n" % "---------"
    hmsg += "%59s<br>" % "---------"
    msg += "%50s %7s\n" % ("Sub Total", subTotalStr.get())
    hmsg += "%50s %7s<br>" % ("Sub Total", subTotalStr.get())
    msg += "%50s %7s\n" % ("Tax", taxStr.get())
    hmsg += "%50s %7s<br>\n" % ("Tax", taxStr.get())
    msg += "%50s %7s\n" % ("Total", totalStr.get())
    hmsg += "%50s %7s<br>" % ("Total", totalStr.get())
    if paidBy.get() == custsale.PaidBy.cash:
        msg += "%50s $%7s\n" % ("Tendered Cash", tenderedStr.get())
        hmsg += "%50s $%7s<br>" % ("Tendered Cash", tenderedStr.get())
        msg += "%50s %7s\n" % ("Returned Cash", returnedStr.get())
        hmsg += "%50s %7s<br>" % ("Returned Cash", returnedStr.get())
    elif  paidBy.get() == custsale.PaidBy.check:
        msg += "%51s %7s\n" % ("Paid by Check #:", checkNumStr.get())
        hmsg += "%51s %7s<br>" % ("Paid by Check #:", checkNumStr.get())
    else:
        msg += "%55s\n" % "Paid by Credit"
        hmsg += "%55s<br>" % "Paid by Credit"

    msg += "\nThank you for your purchase.\n\n\t%s - %s\n" % (et.getBoutiqueName(), et.getEventName())
    hmsg +=  "<br>Thank you for your purchase.<br><br>\t%s - %s</pre>" % (et.getBoutiqueName(), et.getEventName())
    hmsg +=  """\
      </body>
    </html>
    """

    return msg, hmsg


def completeSaleCallback(status, count):
    status.addStatus("Recording Sales Record...")
    date = time.strftime("%m-%d-%Y %H:%M")
    # Create new sale record in sale database
    try:
        check =  int(checkNumStr.get())
    except ValueError:
        check = 0
    cs = custsale.CustSale(custFirstName.get().strip(), custLastName.get().strip(), email.get().strip(),
                           date, paidBy.get(), check)
    if paidBy.get() == custsale.PaidBy.check:
        cs.setCheckNum(checkNumStr.get())
    for i in range(0, itemList.size()):
        cs.addSale(artistList[valListArtistIndex[i]], valListMult[i], valList[i], valListTax[i])
    sf = salefile.SaleFile(et)
    worked, err = sf.record(cs)
    if not worked:
        status.addStatus(err, 'red')
        return 0
    status.addStatus("New sale transaction created: " + sf.transId)

    receiptStr, receiptHTMLStr = createReceipt(sf.lastTransId(), date)
    et.newReceipt(receiptStr)

    if len(emailStr) > 0:
        status.addStatus("Emailing Receipt...")
        result = mail.sendHTMLEMail(et.fromAddr, et.login, et.smtp, et.password, emailStr,
                                et.getBoutiqueName() + " - Sales Receipt", receiptStr, receiptHTMLStr)
        if result != "":
            status.addStatus(result, 'red')

    # print receipt
    if printReceiptInt.get() == 1:
        status.addStatus("Printing Receipt...")
        result = printit.printString(receiptStr)
        if result != "":
            status.addStatus(result, 'red')

    # New Customer? - Add it to the customer data base
    clients.addCustomerRecord(et, custLastName.get().strip(), custFirstName.get().strip(),
                              email.get().strip())
    return 0

# completes the given sale
#   Before doing any database updates handle receipt printing and email so that any failure
#   can be caught and redone without file updates
def completeSale():
    global emailStr
    vendors = []
    vendorVals = []

    emailStr = email.get()
    emailStr.strip()
    el = len(emailStr)
    if emailReceiptInt.get() == 1:
        if  el == 0:
            tkMessageBox.showwarning(
                "Error",
                "Email receipt option selected, but email address field is blank",
                parent=top)
            return False
        if emailStr.find('@') == -1 or emailStr.find(' ') != -1:  # spaces not allowed, must have @
            tkMessageBox.showwarning(
                "Error",
                "Email address (%s) must have @ and no spaces\nPlease fix and retry" % emailStr,
                parent=top)
            return False
    else:
        emailStr = ""

    print "Starting StatusPop..."
    x = completeSaleB.winfo_rootx()+20
    y = completeSaleB.winfo_rooty()-50
    status = statuspopup.StatusPopUp(top, "Sale", completeSaleCallback, x, y)
    print "Exiting StatusPop"
    clearForm()
    return True

def onFindComboSelect(event):
    sel = find.current()
    print sel
    if sel >= 0:
        search.cycle(index=sel)
    updateButtons()

def newCustData():
    fullMatchIndex, nonSelectedIndex, text = search.selection()
    if fullMatchIndex >= 0:
        firstStr.set(flist[fullMatchIndex][0])
        lastStr.set(flist[fullMatchIndex][1])
        emailLStr.set(flist[fullMatchIndex][2])
    else:
        firstStr.set(text)

    updateButtons()


# Create the New Sale UI form
def makeform(frame):
    global artist               # combobox of vendor strings
    global itemList             # The listbox of purchased items
    global valList              # corresponding values for items in itemList
    global valListTax
    global valListMult
    global valListArtistIndex   # corresponding index values into list (vendor list)
    global artistList
    global deleteButton, editButton, addButton
    global lookupButton
    global price                # item cost as entered
    global subTotalStr
    global taxStr
    global totalStr
    global total
    global completeSaleB
    global custFirstName, custLastName
    global email
    global multi
    global paidBy
    global checkNum, checkNumStr
    global emailReceiptInt, printReceiptInt
    global skipUpdateButtons
    global tendered, returned
    global tenderedStr, returnedStr
    global taxable
    global labCost, labTax
    global scrollbar
    global find
    global firstStr, lastStr, emailLStr, flist, search

    totalsIndent = 100
    valList = []
    valListTax = []
    valListMult = []
    valListArtistIndex = []
    skipUpdateButtons = True

    courier = tkFont.Font(family='Courier', size=11, weight='normal')
    # script = tkFont.Font(family='Brush Script MT', size=18, weight='normal')
    script = tkFont.Font(family='Adine Kirnberg', size=26, weight='bold')

    bName = " " + et.getBoutiqueName() + " - " + et.getEventName()
    th = utils.TitleDisplay(frame)

    # Custom info header
    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=5, pady=2)
    lab = Label(row, width=28, text="Customer Information:", anchor='w',fg="grey")
    lab.pack(side=LEFT)

    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=5, pady=2)
    lab = Label(row, text="Find:", anchor='w')
    lab.pack(side=LEFT, padx=5)
    findList = ["First Name", "Last Name", "EMail Addr"]
    find = ttk.Combobox(row, width=15, values=findList, state='readonly')
    find.current(0)
    find.bind("<<ComboboxSelected>>", onFindComboSelect)
    find.pack(side=LEFT, padx=5)
    tip = ToolTip(find, "Select what kind of name to search for")
    flist = clients.loadCustomerData(et)
    search = searchbox.SearchBox(frame, row, dropXFactor=3, dataSet=flist)
    search.pack()
    search.bind_selection(newCustData)
    search.dataPercents = [25,25,50]
    tip = ToolTip(search.sb, "Enter what to search for")

    firstStr = StringVar()  # textvariable is ne
    # customer data (labels, two rows)
    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=5, pady=2)
    lab = Label(row, text="First:", anchor='w')
    lab.pack(side=LEFT, padx=5)
    firstStr = StringVar()  # textvariable is needed to allow callback for changes
    firstStr.trace("w", lambda name, index, sv=firstStr: updateButtons())
    custFirstName = Entry(row, width=18, textvariable=firstStr)
    custFirstName.insert(0, "")
    custFirstName.pack(side=LEFT, padx=5)
    lab = Label(row, text="Last:", anchor='w')
    lab.pack(side=LEFT,padx=5)
    lastStr = StringVar()  # textvariable is needed to allow callback for changes
    lastStr.trace("w", lambda name, index, sv=lastStr: updateButtons())
    custLastName = Entry(row, width=20, textvariable=lastStr)
    custLastName.insert(0, "")
    custLastName.pack(side=LEFT, padx=5)

    # customer data email
    #row = Frame(frame)
    #row.pack(side=TOP, fill=X, padx=5)
    lab = Label(row, text="Email:", anchor='w')
    lab.pack(side=LEFT,padx=5)
    emailLStr = StringVar()  # textvariable is needed to allow callback for changes
    emailLStr.trace("w", lambda name, index, sv=emailLStr: updateButtons())
    email = Entry(row, width=28, textvariable=emailLStr)
    email.insert(0, "")
    email.pack(side=LEFT, padx=5)
    lookupButton = ttk.Button(row, text='Lookup...', state=NORMAL,
                          command=(lambda: lookupButtonCall()))
    lookupButton.pack(side=LEFT, padx=10)
    tip = ToolTip(lookupButton, "Use the First, Last, and Email data to lookup existing cutomer")

    separator = Frame(frame, height=5, bd=5, relief=SUNKEN)
    separator.pack(side=TOP, fill=X, padx=5, pady=7)

    # Sale entry/display information header
    row = Frame(frame)
    lab = Label(row, width=28, text="Sale Entry:", anchor='w',fg="grey")
    row.pack(side=TOP, fill=X, padx=5, pady=0)
    lab.pack(side=LEFT)

    # sale: artist, multiplier
    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=10, pady=2)
    lab = Label(row, width=12, text="Artist/Vendor:", anchor='e')
    lab.pack(side=LEFT)
    al.readEventFile()
    artistList, dummy1, dummy2, taxable = al.createLists(askParticipating=True)
    artist = ttk.Combobox(row, width=35, values=artistList, state='readonly', foreground='blue')
    artist.bind("<<ComboboxSelected>>", onComboSelect)
    artist.pack(side=LEFT, padx=5)
    tip = ToolTip(artist, "Select artist/vendor for this item sale")
    # artist.current(0)                 # first value default, rather than none
    lab = Label(row, text=" X", anchor='w')
    lab.pack(side=LEFT)
    multiplier = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    multi = ttk.Combobox(row, width=2, values=multiplier, state='readonly')
    multi.current(0)                 # first value default, rather than none
    multi.pack(side=LEFT,padx=5)
    tip = ToolTip(multi, "How many of this same item")

    # sale cost
    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=10, pady=2)
    lab = Label(row, width=12, text="Cost:", anchor='e')
    lab.pack(side=LEFT)
    costLStr = StringVar()  # textvariable is needed to allow callback for changes
    costLStr.trace("w", lambda name, index, sv=costLStr: updateButtons())
    price = Entry(row, width=10, justify=RIGHT, textvariable=costLStr)
    price.insert(0, ".00")
    price.pack(side=LEFT, padx=5)
    tip = ToolTip(price, "Item Cost")
    addButton = ttk.Button(row, text='Add Sale Item', state=DISABLED, command=lambda: addSaleItem())
    addButton.pack(side=LEFT, padx=20)
    tip = ToolTip(addButton, "Register/Add this sale item")
    b1 = ttk.Button(row, text='Clear Sale Fields', command=lambda: clearFields())
    b1.pack(side=LEFT, padx=20)
    tip = ToolTip(b1, "Cancel/Clear these sale item fields")

    # Sale Items
    row = Frame(frame)
    lab = Label(row, width=48, text="Sale Items by Artist/Vendor:", anchor='w')
    row.pack(side=TOP, fill=X, padx=10, pady=5)
    lab.pack(side=LEFT)
    # lab = Label(row, width=22, text="Item Description:", anchor='w')
    # lab.pack(side=LEFT)
    labCost = Label(row, width=10, text="Cost:", anchor='w')
    labCost.pack(side=LEFT)
    labTax = Label(row, width=3, text="Tax:", anchor='w')
    labTax.pack(side=LEFT)

    # Sales Items , scrollbar and delete button
    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=15, pady=2)
    scrollbar = Scrollbar(row)
    itemList = Listbox(row, width=51, height=7, selectmode=SINGLE, font=courier,
                       activestyle='none', fg='blue', yscrollcommand=scrollbar.set)
    itemList.bind('<<ListboxSelect>>', onItemSelect)
    tip = ToolTip(itemList, "List of items for this sales transaction")
    scrollbar.config(command=itemList.yview)
    deleteButton = ttk.Button(row, text='Delete\nItem', state=DISABLED,
                          command=(lambda: deleteItemButton()))
    tip = ToolTip(deleteButton, "Delete the selected item")
    itemList.pack(side=LEFT)
    scrollbar.pack(side=LEFT, fill=BOTH)
    deleteButton.pack(side=TOP, padx=5)
    editButton = ttk.Button(row, text='Edit\nItem', state=DISABLED,
                          command=(lambda: editItemButton()))
    editButton.pack(side=TOP, padx=5,pady=5)
    tip = ToolTip(editButton, "Edit the selected item")


    # subtotal
    row = Frame(frame)
    #lab = Label(row, width=4, text="")
    row.pack(side=TOP, padx=0,pady=3, fill=X)
    #lab.pack(side=LEFT, padx=0)

    lab = Label(row, width=12, text="Sub Total:", anchor='e')
    subTotalStr = StringVar()
    subTotal = Label(row, width=8, text="$0.00", borderwidth=1, relief=GROOVE, bg="white", anchor="e",
                     textvariable=subTotalStr)
    lab.pack(side=LEFT, padx=2)
    subTotal.pack(side=LEFT, padx=2)

    taxLabelStr = "Tax %3.2f%%:" % (et.taxRate * 100)
    lab = Label(row, width=8, text=taxLabelStr, anchor='e')
    taxStr = StringVar()
    tax = Label(row, width=8, text="$0.00", borderwidth=1, relief=GROOVE, bg="white", anchor="e",
                textvariable=taxStr)
    lab.pack(side=LEFT, padx=5)
    tax.pack(side=LEFT, padx=2)

    # totals row
    lab = Label(row, width=5, text="Total:", anchor='e', )
    totalStr = StringVar()
    total = Entry(row, width=9, text="$0.00", bg="white", justify=RIGHT,
                  textvariable=totalStr)
    total.bind("<Key>", lambda e: "break")
    #Entry(row, width=8, justify=RIGHT, textvariable=tenderedStr)
    lab.pack(side=LEFT, padx=0)
    total.pack(side=LEFT, padx=5)
    #total.place(x=400, y=1)

    row = Frame(frame)
    row.pack(side=TOP, padx=0, pady=0, fill=X)
    lab = Label(row, width=47, text="Tendered Cash:", anchor='e')
    tenderedStr = StringVar()
    tenderedStr.trace("w", lambda name, index, sv=tenderedStr: calcUpdate())
    tendered = Entry(row, width=9, justify=RIGHT, textvariable=tenderedStr)
    tendered.insert(0, ".00")
    lab.pack(side=LEFT, padx=0)
    tendered.pack(side=LEFT, padx=5)
    # tendered.place(x=370, y=1)


    row = Frame(frame)
    #lab = Label(row, width=10, text="")
    row.pack(side=TOP, padx=0, fill=X)
    #lab.pack(side=LEFT, padx=0)
    lab = Label(row, width=47, text="Returned Cash:", anchor='e')
    returnedStr = StringVar()
    returned = Entry(row, width=9, bg="white", justify=RIGHT,
                     textvariable=returnedStr)
    returned.bind("<Key>", lambda e: "break")
    lab.pack(side=LEFT, padx=0)
    returned.pack(side=LEFT, padx=5)
    # returned.place(x=400, y=1)



    separator = Frame(frame, height=5, bd=5, relief=SUNKEN)
    separator.pack(side=TOP, fill=X, padx=5, pady=10)

    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=5,pady=10)
    lab = Label(row, width=6, text="Paid by:", anchor='w')
    lab.pack(side=LEFT,padx=10)
    paidBy = IntVar()
    paidBy.set(custsale.PaidBy.cash)
    caRButton = ttk.Radiobutton(row, text="Cash", variable=paidBy, value=1, command=(lambda: doUpdates()))
    caRButton.pack(side=LEFT, anchor='w')
    ckRButton = ttk.Radiobutton(row, text="Check", variable=paidBy, value=2, command=(lambda: doUpdates()))
    ckRButton.pack(side=LEFT, anchor='w')
    lab = Label(row, width=1, text="#:", anchor='w')
    lab.pack(side=LEFT, padx=5)
    checkNumStr = StringVar()  # textvariable is needed to allow callback for changes
    checkNumStr.trace("w", lambda name, index, sv=checkNumStr: updateButtons())
    checkNum = ttk.Entry(row, width=8, state=DISABLED, textvariable=checkNumStr)
    checkNum.insert(0, "")
    checkNum.pack(side=LEFT)
    ccRButton = ttk.Radiobutton(row, text="Credit\nCard", variable=paidBy, value=3, command=(lambda: doUpdates()))
    ccRButton.pack(side=LEFT, padx=5, anchor='w')
    emailReceiptInt = IntVar()
    emailReceipt = Checkbutton(row, text="Email\nReceipt", variable=emailReceiptInt)
    emailReceipt.select()
    emailReceipt.pack(side=LEFT, padx=20)
    # Print Receipt is not wanted for now
    if et.allowPrintReceipt:
        state = NORMAL
    else:
        state = DISABLED
    printReceiptInt = IntVar()
    printReceipt = Checkbutton(row, text="Print\nReceipt", state=state, variable=printReceiptInt)
    printReceipt.pack(side=LEFT, padx=5)


    # complete sale button
    completeSaleB = ttk.Button(row, text='  Complete Sale...  ', state=DISABLED,
                               command=(lambda: completeSale()))
    completeSaleB.pack(side=RIGHT, padx=10)

    # row = Frame(frame, height=5, bd=5, relief=SUNKEN)
    # row.pack(side=LEFT, fill=X, padx=5, pady=5)

    separator = Frame(frame, height=5, bd=5, relief=SUNKEN)
    separator.pack(side=TOP, fill=X, padx=5, pady=10)

    # bottom buttons (close and clear form)
    row = Frame(frame)
    b2 = ttk.Button(row, text='Clear Form', command=lambda: clearForm())
    b3 = ttk.Button(row, text='Close', command=frame.quit)
    row.pack(side=BOTTOM, fill=X, padx=10, pady=5)
    b2.pack(side=LEFT, padx=5)
    b3.pack(side=RIGHT)

    skipUpdateButtons = False
    custFirstName.focus_force()
    top.iconbitmap('myicon.ico')
    top.update()
    th.showHeader(bName)
    updateButtons()
    # Need to manipulate placement of some items due to diff in Win7 versus Win10
    # print "g",total.winfo_geometry(), type(total.winfo_geometry())
    x, width = locX(total.winfo_geometry())
    tendered.place(x=x, y=1)
    returned.place(x=x, y=1)
    x, width = locX(scrollbar.winfo_geometry())
    split = (x / 5)
    labCost.place(x=split*4, y=0)
    labTax.place(x=split*4+55, y=0)
    frame.mainloop()


# catch top right [x] close event
def onClosing():
    top.quit()


def newSale(eventTracker):
    global top
    global al
    global et

    et = eventTracker
    # read in the artist data
    al = artistlist.ArtistList(et)
    if not al.readFile(showError=True):
        return
    top = Toplevel()
    top.resizable(0,0)
    top.focus_force()

    top.wm_title(et.appName() + ": New Sale")
    top.protocol("WM_DELETE_WINDOW", onClosing)  # Handle the [x] event
    makeform(top)
    top.destroy()


