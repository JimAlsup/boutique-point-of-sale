'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

import utils
# from openpyxl import load_workbook
import portalocker
import os
import json
import cryptstr

# Dictionary of name to value terms used used as .ini type program data
# Data is stored using a json library

loginStr = 'login'
nameStr = 'name'
eventNameStr = 'event_name'
taxStr = 'tax'
fromAddrStr = 'from_addr'
passwordStr = 'password'
smtpStr = 'smtp'
allowPrintReceiptStr = 'allow_print'

json_misc_data = {
    nameStr: 'Not Defined',
    eventNameStr: '',
    taxStr: 8.25,
    fromAddrStr: "",
    loginStr: "",
    passwordStr: "",
    smtpStr: "",
    allowPrintReceiptStr: False
}


class EventTrack:
    def __init__(self):
        self._topLevelDir = ""
        self._receiptFileName = "receipt.log"              # lives in top level directory
        self._artistListFileName = "artists.xlsx"          # lives in top level directory
        self.artistListTemplateFileName = "artists_template.xlsx"  # lives in top level directory
        self._eventArtistsFileName = "event_artists.xlsx"  # lives in event sub directory
        self._saleRecordFileName = "sale_records.xlsx"     # lives in event sub directory
        self._masterFileName = "data/master_dir.txt"       # lives in the data subdir of the program directory
        self._transactionFileName = "transaction_no.txt"   # lives in the top level directory
        self._miscFileName = "misc_data.json"              # lives in the top level directory
                                                           # tracks boutique name, current event, etc...
        self._customerDatabaseFileName = "clientes.xlsx"   #
        self.customerDatabaseTemplateFileName = "clientes_template.xlsx"   #
        self._boutiqueName = ""
        self._reinit()
        self.debug = False  # allows debug statements to be printed


    def _reinit(self):
        self._transactionId = 1000
        self.idActive = False
        self.file = 1
        self._events = []
        self._selectedEvent = -1
        self.miscData = json_misc_data
        self._readMasterFile()
        self._readMiscFile()


    def appName(self):
        return "Boutique Point of Sale"

    def transactionFileName(self):
        return utils.pathCombine(self._topLevelDir, self._transactionFileName)

    def artistsListFileName(self):
        return utils.pathCombine(self._topLevelDir ,self._artistListFileName)

    def artistsEventDirectory(self):
        if self._selectedEvent != -1:
            e = self._events[self._selectedEvent]
        else:
            return ""
        return utils.pathCombine(self._topLevelDir, e)

    def eventArtistsFileName(self):
        if self._selectedEvent != -1:
            e = self._events[self._selectedEvent]
        else:
            return ""
        return utils.pathCombine(self._topLevelDir, e, self._eventArtistsFileName)

    def receiptFileName(self):
        e = self._events[self._selectedEvent]
        return utils.pathCombine(self._topLevelDir, e, self._receiptFileName)

    def saleRecordFileName(self):
        e = self._events[self._selectedEvent]
        return utils.pathCombine(self._topLevelDir, e, self._saleRecordFileName)

    def customerDatabaseFileName(self):
        return utils.pathCombine(self._topLevelDir, self._customerDatabaseFileName)

    def topLevelDir(self):
        return self._topLevelDir

    def selectedEventIndex(self):
        return self._selectedEvent

    def selectedEventDir(self):
        if self._selectedEvent == -1:
            return ""
        return self._events[self._selectedEvent]

    def events(self):
        return self._events

    def createEvent(self, name):
        # Create directory based on event name
        dir = utils.pathCombine(self.topLevelDir(), name)
        if os.path.exists(dir):
            return "Event Name/Path already exists"
        # create event master file in event directory
        try:
            os.mkdir(dir)
        except OSError:
            return "Event/Path creation failed."
        # No default files are needed in the event directory, but
        # we must update others value in artistlist
        self._events.append(name)
        self._selectedEvent = len(self._events)-1
        self._readMasterFile()
        return ""

    def _readMasterFile(self):
        self._topLevelDir = ""
        try:
            f = open(self._masterFileName, 'r')
        except IOError:
            print "Warning:", self._masterFileName, "not found."
            return
        line = f.readline()
        line = line[0:len(line)-1]
        # do we need to validate the directory?
        print "Directory", line, "is value:", os.path.exists(line)
        self._topLevelDir = line
        f.close()
        return

    def updateMasterFile(self, dir):
        self._topLevelDir = dir
        # do we need to validate the directory?
        try:
            f = open(self._masterFileName, 'w')
        except IOError:
            print "Warning:", self._masterFileName, "not found."
            return
        try:
            # f.seek(0)
            f.write(dir + "\n")
        except IOError:
            f.close()
            return
        f.close()

        # Things have changed and we need to reinit
        self._reinit()

    def _parseMiscData(self):
        self._boutiqueName = self.miscData[nameStr]
        en = self.miscData[eventNameStr]
        i = 0
        self._selectedEvent = -1
        for l in self._events:
            if l == en:
                self._selectedEvent = i
                break
            i += 1
        if self._selectedEvent == -1:
            print 'warning: no selected event found'
        self.taxRate = self.miscData[taxStr] / 100
        self.fromAddr = self.miscData[fromAddrStr]
        self.login = self.miscData[loginStr]
        self.smtp = self.miscData[smtpStr]
        self.password = cryptstr.decryptStr(self.miscData[passwordStr])
        try:
            self.allowPrintReceipt = self.miscData[allowPrintReceiptStr]
        except KeyError:
            self.allowPrintReceipt = False


    def _readMiscFile(self):
        self._parseMiscData()  # Set default values
        self._events = utils.listDirs(self.topLevelDir())
        fn = utils.pathCombine(self.topLevelDir(), self._miscFileName)
        try:
            with open(fn, 'r') as f:
                try:
                    self.miscData = json.load(f)
                except ValueError:
                    print "Invalid data in json file, is %s a json file?" % fn
                    return
        except:
            print "File %s not found" % fn
            return
        self._parseMiscData()  #


    def updateMiscFile(self):
        fn = utils.pathCombine(self.topLevelDir(), self._miscFileName)
        # use a json file for easy key data pairing
        # update miscData structure with latest values
        self.miscData[nameStr] = self._boutiqueName
        self.miscData[eventNameStr] = self.getEventName()
        self.miscData[taxStr] = self.taxRate * 100
        self.miscData[fromAddrStr] = self.fromAddr
        self.miscData[loginStr] = self.login
        self.miscData[smtpStr] = self.smtp
        self.miscData[passwordStr] = cryptstr.encryptStr(self.password)
        self.miscData[allowPrintReceiptStr] = self.allowPrintReceipt
        try:
            with open(fn, 'w') as f:
                json.dump(self.miscData, f)
            f.close()
        except IOError:
            print "Error: could not write/create json file:", fn

    def getBoutiqueName(self):
        return self._boutiqueName

    def setBoutiqueName(self, name):
        self._boutiqueName = name
        self.updateMiscFile()

    def getEventName(self):
        if self._selectedEvent < 0 or self._selectedEvent >= len(self._events):
            return ""
        return self._events[self._selectedEvent]

    def setEventIndex(self, eventIndex):
        if eventIndex > len(self._events):
            print "Attempt to set event index > # of events"
            return
        self._selectedEvent = eventIndex
        self.updateMiscFile()

    '''
    Transaction locking is key part of making the excel databased work over
    multiple PCs via shared directory.  The API below provides a lock service
    and a transaction ID value associated d.  No lock should be needed to
    simly read the files.  The API:

        startTransaction(): Blocks until the lock file is free.  Once obtained
            the file is locked and the transaction id is updated in file
        getTransactionId(): Returns the transaction Id as a string.  Normally
            this should not be needed.
        endTransaction():   Unlocks the file

     A debug flag can be set to enable transitional output.
    '''

    def getTransactionId(self, sale=None):
        if sale is None:
            sale = True  # False means a refund transaction
        if self.idActive:
            #print sale, int(sale), "T%04d%s" % (self.transactionId, "RS"[sale])
            return "T%04d%s" % (self._transactionId, "RS"[sale])
        return ""


    # Return a new Transaction id
    def startTransaction(self):
        if self.idActive:
            return False
        try:
            self.file = open(self.transactionFileName(), 'r+')
        except IOError:
            if self.debug:
                print ">> read+ open failed"
            try:
                self.file = open(self.transactionFileName(), 'w+')
            except IOError:
                print "<Error on event transaction lock file>", self.transactionFileName()
                return False
        if self.debug:
            print '<<waiting for lock to open>>'
        portalocker.lock(self.file, portalocker.LOCK_EX)
        if self.debug:
            print "<<got lock and locking it>>"
        try:
            cur = self.file.readline()
        except IOError:
            cur = ""
        if cur == "":  # must be new file
            if self.debug:
                print "First Transaction"
            self._transactionId = 0
        else:
            self._transactionId = int(cur)
        self.idActive = True
        self._transactionId += 1
        s = "%04d" % self._transactionId
        try:
            self.file.seek(0)
            self.file.write(s)
            self.file.write('\n')
            os.fsync(self.file)  # this flush is necessary or the waiting caller will read the old value
        except:
            print "<Error on transaction/lock file write>"
            return False
        if self.debug:
            print "Transaction Id: T" + s + " created."
        return True


    def endTransaction(self):
        if ( self.idActive ):
            if self.debug:
                print '<<unlockeding>>'
            try:
                self.file.close()
            except IOError:
                print "IOError: on close"
            if self.debug:
                print '<<unlocked>>'
            self.idActive = False

    def newReceipt(self, receipt):
        # Update master receipt file (text file to allow later receipt lookup)
        with open(self.receiptFileName(), "a") as receiptFile:
            receiptFile.write(receipt)
            receiptFile.write("---------------------------------------------------------------------------\n")
            receiptFile.close()



if __name__ == '__main__':
    # This unit test
    et = EventTrack()
    et.debug = True
    et.startTransaction()
    s = raw_input("Before answering prompt, start another process of this app>> ")
    et.endTransaction()
