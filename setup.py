from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [], excludes = [])

import sys
base = 'Win32GUI' if sys.platform=='win32' else None

executables = [
    Executable('pos.py', base=base, icon='myicon.ico')
]

setup(name='pos',
      version = '0.1',
      description = 'Boutique Point of Sale',
      options = dict(build_exe = buildOptions),
      executables = executables)
