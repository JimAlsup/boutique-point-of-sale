'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

from Tkinter import *
import tkMessageBox
import ttk
import artistlist


MasterArtistListFileName = "artistlist.txt"


def cancel():
    global cancelled
    cancelled = True
    # print "cancelled"
    top.quit()


def killTopWindow():
    global cancelled
    cancelled = True
    # print "Killed top window"
    top.quit()
    # no need to destroy the window.  t.quit exits via the mainloop
    # and this has a destory


def updateButtons():
    # print "Update Buttons ", costLStr.get()
    if skipUpdateButtons:
        return
    # This function gets call during construction time when some gui items are not setup.
    # avoid this with this boolean
    #if skipUpdateButtons:
        #return

    err = False
    value = 0
    try:
        value = float(costLStr.get())
    except:
        err = True
    if err or value == 0:
        okButton.config(state=DISABLED)
    else:
        okButton.config(state=NORMAL)


def edit(master, vendors, vindex, xfactor, value):
    global top
    global cancelled
    global okButton
    global skipUpdateButtons
    global costLStr

    cancelled = False
    skipUpdateButtons = True

    frame = Toplevel()
    top = frame
    top.resizable(0,0)
    frame.wm_title("Edit Item")

    # Sale Items
    row=Frame(frame)
    row.pack(side=TOP, fill=X, padx=5, pady=2)
    lab = Label(row, width=70, text="Sale Item:", anchor='w')
    lab.pack(side=LEFT)
    # sale item and cost labels
    row = Frame(frame)
    lab = Label(row, width=35, text="Select Artist/Vendor:", anchor='w')
    row.pack(side=TOP, fill=X, padx=10, pady=2)
    lab.pack(side=LEFT)
    lab = Label(row, width=6, text="X", anchor='w')
    lab.pack(side=LEFT)
    lab = Label(row, width=20, text="Cost:", anchor='w')
    lab.pack(side=LEFT)

    # sale artist, multiplier(X) and cost
    row = Frame(frame)
    artist = ttk.Combobox(row, width=35, values=vendors, state='readonly', foreground='blue')
    artist.current(vindex)                 # first value default, rather than none
    multiplyer = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    multi = ttk.Combobox(row, width=2, values=multiplyer, state='readonly')
    multi.current(xfactor-1)                 # first value default, rather than none
    costLStr = StringVar()  # textvariable is needed to allow callback for changes
    costLStr.trace("w", lambda name, index, sv=costLStr: updateButtons())
    cost = Entry(row, width=10, justify=RIGHT, textvariable=costLStr)
    cost.insert(0, "%6.2f" % value )
    row.pack(side=TOP, fill=X, padx=10, pady=2)
    artist.pack(side=LEFT, padx=5)
    multi.pack(side=LEFT, padx=5)
    cost.pack(side=LEFT, padx=0)

    # bottom buttons (cancel and ok)
    row = Frame(frame)
    b2 = Button(row, text=' Cancel ', command=lambda: cancel())
    okButton = Button(row, text='   OK   ', command=frame.quit, fg="blue")
    row.pack(side=BOTTOM, fill=X, padx=10, pady=10)
    b2.pack(side=LEFT, padx=5)
    okButton.pack(side=RIGHT)

    skipUpdateButtons = False

    frame.protocol("WM_DELETE_WINDOW", killTopWindow)  # Handle the [x] event
    # frame.transient(master)
    frame.grab_set()
    # master.wait_window(master)
    frame.mainloop()
    aindex = artist.current()
    xfactor = int(multi.get())
    value = float(costLStr.get())
    frame.destroy()
    if cancelled:
         return 0,0,0
    # vendor index, x factor, cost
    return aindex, xfactor, value

def kill():
    # print "App killed"
    root.destroy()

if __name__ == "__main__":
    al = artistlist.ArtistList( MasterArtistListFileName )
    if not al.readFile:
        print "Unable to load artist list file"
        exit(0, 0)

    root = Tk()
    root.title("Edit Sale Item")
    root.withdraw()
    value = 4.0
    vendor, xfactor, price = edit(root, al, 2, 1, 4.0)
    # print vendor, xfactor, "%4.2f" % price
    root.protocol("WM_DELETE_WINDOW", kill)
    root.deiconify()
    root.destroy()

    root.mainloop()
