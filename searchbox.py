'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

from Tkinter import *
import ttk
import tkFont

# define some char/keycode constants
class KeySym():
    Down = 40
    Up = 38
    ESC = 27

class SearchBox(object):
    # width is in characters, height is in rows, dropXFactor is a multiplier for width to widen the drip height
    def __init__(self, master, row, width=None, height=None, dropXFactor=None, dataSet=None):
        self._width = width or 28
        self._height = height or 6
        self._master = master
        self.newTop = None

        if dropXFactor is None:
            self._dropXFactor = 1
        else:
            self._dropXFactor = dropXFactor
        self._dataSet = dataSet
        self.row = row
        self.sbStr = StringVar()
        self.sbStr.trace("w", lambda name, index, sv=self.sbStr: self._onSearchBoxChanged())
        self.sb = Entry(row, width=self._width, textvariable=self.sbStr)
        self.sb.bind("<Key>", self._keySB)
        self.sb.bind('<FocusIn>', self._onSBFocusIn)
        self.sb.bind('<FocusOut>', self._onSBFocusOut)
        # create drop/collapse button
        # todo: add an image (up and down) instead of 'v'
        self.dropButton = ttk.Button(row, text='v', width=1, command=self._onDropButton)
        self.lb = None
        self.sbList = []
        self.dIndex = None
        self.dataPercents = None
        self._skipUpdate = False
        self._restoreAsViewable = True  # Tracks drop ListBox state for usage by drop/collapse button
        self._lbMouseActive = False  # True when mouse in drop ListBox and used to prevent closing
                                     # of same box when focus is forced back to search box
        self._callback = None

    def pack(self):
        self.sb.pack(side=LEFT, padx=0)
        self.dropButton.pack(side=LEFT, padx=0)

    def _onDropButton(self):
        # At this point the Entry box lost focus.  We don't focus
        # to ever reside with the drop down button so put focus back
        # on the search box.
        # To implement the drop state change we first reset this class
        # variable.
        # print "vs", self.restoreAsViewable
        self._restoreAsViewable = not self._restoreAsViewable
        self.sb.focus_set()

    def _onSBFocusIn(self, event):
        # print self.lb.curselection(), self.lb, self.lb.focus_get()
        if not self.lb is None:
            # print "focus in - setting", self._restoreAsViewable
            self._activateList(show=self._restoreAsViewable)
            self.newTop.lift()

    def _onSBFocusOut(self, event):
        # print self.lb.curselection(), self.lb, self.lb.focus_get()
        if not self.lb is None:
            # print "focus out, state is", self.newTop.winfo_viewable(), "lb mouse active:", self._lbMouseActive
            self._restoreAsViewable = self.newTop.winfo_viewable()
            if self._lbMouseActive:
                return
            self._activateList(show=False)

    def _onLBFocusIn(self, event):
        if self.lb == self.lb.focus_get():
            # print "focus needs changing", self._restoreAsViewable
            # self.restoreAsViewable = not self.restoreAsViewable
            self.sb.focus_set()

    # on LB item select
    def _onItemSelect(self, event):
        sel = self.lb.curselection()
        # print "[ sel", sel,
        if len(sel) == 0:
            return
        # move the selection text into the search box
        # indecies below are sel index, 1=sbList index, 0=dataSet column 0 data
        if type(self._dataSet[0]) is tuple:
            # print self.sbList[sel[0]]
            n = self._dataSet[self.sbList[sel[0]][1]][self.dIndex]
        else:
            n = self._dataSet[self.sbList[sel[0]][1]]
        # print "assigning", n
        self._skipUpdate = True  # we don't want the window changing for this
        self.sbStr.set(n)
        # set selection to end of content
        self.sb.icursor(END)
        self._skipUpdate = False
        if not self._callback is None:
            self._callback()
        # print "end sel"

    def _onLBEnter(self, event):
        # print "LBMouseIn"
        self._lbMouseActive = True

    def _onLBLeave(self, event):
        # print "LBMouseOut"
        self._lbMouseActive = False

    def _onLBButtonRelease1(self, event):
        # print "BRelease"
        self._activateList(show=False)

    def _onMasterConfigure(self, event):
        # print "C ET:", event.type
        # event.type is a str
        if event.type == "22":   # restore after min min
            # Every time we get this event reset the window level and pos
            if self._restoreAsViewable:   # only if it was active/viewable
                self.newTop.deiconify()
                self.newTop.lift()
            pos = "%+d%+d" % (self.sb.winfo_rootx(),
                              self.sb.winfo_rooty()+self.sb.winfo_height())
            self.newTop.geometry(pos)

    def _onMasterMinimize(self, event):
        # print "M ET:", event.type
        if event.type == "18":
            # print "hiding"
            self.newTop.withdraw()

    def _activateList(self, create=None, show=None):
        rows = len(self.sbList)
        if rows == 0 and create is None:
            # self.beep()
            return
        if self.lb is None:
            if self._dataSet is None:
                return
            if self.dIndex is None:
                self.dIndex = 0
            self.ffont = tkFont.Font(family="Helvetica", size=10)
            self.newTop = Toplevel(self._master)
            self.newTop.overrideredirect(True)
            self.scrollbar = Scrollbar(self.newTop)
            w = (self.sb.winfo_width()*self._dropXFactor) / self.ffont.measure("a")
            # print "d", self.sb.winfo_width(), self._dropXFactor, self.ffont.measure("a"), w
            self.lb = Listbox(self.newTop, width=w, height=self._height,
                              selectmode=SINGLE, bd=2, relief=GROOVE, activestyle='none',
                              state=NORMAL, yscrollcommand=self.scrollbar.set, font=self.ffont)
            self.scrollbar.config(command=self.lb.yview)
            self.lb.bind('<<ListboxSelect>>', self._onItemSelect)
            self.lb.bind('<FocusIn>', self._onLBFocusIn)
            self.lb.bind('<Enter>', self._onLBEnter)
            self.lb.bind('<Leave>', self._onLBLeave)
            self.lb.bind('<ButtonRelease-1>', self._onLBButtonRelease1)

            self.lb.pack(side=LEFT)
            self.scrollbar.pack(side=LEFT, fill=Y, pady=2)
            # position the drop down windowo to below the edit box
            pos = "%+d%+d" % (self.sb.winfo_rootx(),
                              self.sb.winfo_rooty()+self.sb.winfo_height())
            self.newTop.geometry(pos)
            self.lb.bind("<Key>", self._keyLB)
            self._master.bind("<Configure>", self._onMasterConfigure)
            self._master.bind("<Unmap>", self._onMasterMinimize)
            self.newTop.wm_attributes("-topmost", 1)
            self.newTop.withdraw()
        else:
            # print "a:", self.newTop.winfo_viewable(),
            # if show is None:
            #     print "no show"
            # else:
            #     print "show =", show
            if self.newTop.winfo_viewable():
                if show is None or not show:
                    # print "a off"
                    self.newTop.withdraw()
            else:
                if show is None or show:
                    # print "a on"
                    self.newTop.deiconify()

    def _keyLB(self, event):
        # print "keyLB:", repr(event.char), event.keycode
        return


    def _keySB(self, event):
        # print "keySB:", repr(event.char), event.keycode
        #if len(self.tree.selection()) == 0:
        #    self.searchBox.place_forget()
        #    self.container.bell()
        #    return
        if event.char == '\r':
            sel = self.lb.curselection()
            if not self.lb is None and self.newTop.winfo_viewable():
                self._activateList(show=False)  # take down the list if active
            if not self._callback is None:
                self._callback()
            if len(sel) == 0:
                return  # nothing to do here
            return      #self.  # does not return unless user supplied
        if event.keycode == KeySym.ESC:
            if not self.lb is None and self.newTop.winfo_viewable():
                # restore original value before change from listbox
                self._activateList(show=False)
            return
        if event.keycode == KeySym.Down:
            if self.newTop is None or not self.newTop.winfo_viewable():
                self.beep()
                return
                # move the selection down
            sel = self.lb.curselection()
            # print "s", sel, self.lb.size()
            new = -1
            if len(sel) == 0:
                new = 0
            elif sel[0] >= 0 and sel[0] < self.lb.size()-1:
                self.lb.select_clear(sel)
                new = sel[0]+1
            if new >= 0:
                self.lb.select_set(new)
                self.lb.see(new)
                # update the search box text
                self._skipUpdate = True
                if type(self._dataSet[0]) is tuple:
                    n = self._dataSet[self.sbList[new][1]][self.dIndex]
                else:
                    n = self._dataSet[self.sbList[new][1]]
                self.sbStr.set(n)
                self.sb.icursor(END)
                self._skipUpdate = False
                if not self._callback is None:
                    self._callback()

            return
        if event.keycode == KeySym.Up:
            if self.newTop == None or not self.newTop.winfo_viewable():
                self.beep()
                return
            sel = self.lb.curselection()
            # print "s", sel, self.lb.size()
            if sel[0] > 0:
                self.lb.select_clear(sel)
                new = sel[0]-1
                self.lb.select_set(new)
                self.lb.see(new)
                # update the search box text
                self._skipUpdate = True
                if type(self._dataSet[0]) is tuple:
                    n = self._dataSet[self.sbList[new][1]][self.dIndex]
                else:
                    n = self._dataSet[self.sbList[new][1]]
                self.sbStr.set(n)
                self.sb.icursor(END)
                self._skipUpdate = False
                if not self._callback is None:
                    self._callback()

    def _tuple0(self, item):
        return item[0].lower()

    def _onSearchBoxChanged(self):
        # print "sbc>", self.sbStr.get(), "sU", self._skipUpdate
        if self._skipUpdate:
            return
        # print "Updating LB"
        # Erase any existing search match and create a new one
        if not self.lb is None:
            self.lb.delete(0, END)
        self.lastTyped = self.sbStr.get()
        self.sbList = self._searchIt()
        self.sbList.sort(key=self._tuple0)
        # print "List", list
        if len(self.sbList) == 0:
            if not self.lb is None:
                self.newTop.withdraw()
            return
        for name, index in self.sbList:
            self.lb.insert(END, name)
        # make the list viewable
        if not self.newTop.winfo_viewable():
            self._activateList()

    def beep(self):
        self._master.bell()
        return

    def _searchIt(self):
        list = []
        finding = self.sbStr.get().lower()
        if finding == "":
            return list
        i = 0
        self._activateList(create=True)
        for name in self._dataSet:
            # print "[",name,len(name), type(name)
            if type(name) == tuple:
                rname = name[self.dIndex].lower()
            else:
                rname = name
            if rname.find(finding) == 0:
                # print "match",
                if type(name) == tuple:
                    fullLen = int(self.sb.winfo_width() * (self._dropXFactor))
                    if self.dataPercents is None:
                        self.dataPercents = [100/len(name)] * len(name)
                    assert(len(name) == len(self.dataPercents))
                    font = self.ffont
                    spaceLen = font.measure(" ")
                    # start with selected tuple item
                    n = name[self.dIndex]
                    lname = n + ','
                    fillVal = int(fullLen*(self.dataPercents[self.dIndex]/100.0) - self.ffont.measure(n))
                    for x in range(len(name)):
                        if x == self.dIndex:   # skip over the one we already entered first
                            continue
                        n = name[x]
                        lname += " " * (fillVal/spaceLen)
                        # print "m", self.ffont.measure(lname),
                        lname += n + ','
                        fillVal = int(fullLen*(self.dataPercents[x]/100.0) - self.ffont.measure(n))
                    lname = lname[0:-1]
                    # print "f", self.ffont.measure(lname)
                else:
                    lname = name
                list.append((lname, i))
            # print "]"
            i += 1
        return list

    def _findIt(self):
        finding = self.sbStr.get().lower()
        if finding == "":
            return list
        i = 0
        for name in self._dataSet:
            # print "[",name,len(name), type(name)
            if type(name) == tuple:
                rname = name[self.dIndex].lower()
            else:
                rname = name
            if rname == finding:
                return i, rname
            i += 1
        return -1, ""

    # Returns:
    #   -1,    -1, text - no match
    #   -1,   >-0, text - match with index to DataSet, but user did not select it
    #    >=0, >=0, text - full match with index to dataSet
    def selection(self):
        #
        if not self.lb is None:
            sel = self.lb.curselection()
            dataIndex = self._findIt()[0]
        else:
            sel = []
            dataIndex = -1
        text = self.sbStr.get()
        if len(sel) == 0:
            # -1, -1, text - no match
            # -1, >-0, text - match, but user did not select it
            return -1, dataIndex, text
        return dataIndex, dataIndex, text

    def bind_selection(self, callback):
        self._callback = callback
        # print "test cb", self._callback()


    def cycle(self, index=None):
        if self._dataSet is None:
            return
        if type(self._dataSet[0]) is tuple:
            l = len(self._dataSet[0])
            if index is None:
                self.dIndex += 1
            else:
                assert( index < l )
                self.dIndex = index
            if self.dIndex >= l:
                self.dIndex = 0
            self.sbStr.set(self.sbStr.get())

# Main entry point for test

def gotit():
    i, j, t = tup.selection()
    selUpdate['text'] = t
    # print "gotit"

if __name__ == '__main__':
    root = Tk()
    root.resizable(0, 0)
    root.title( "Search Box Test" )
    row = Frame()
    row.pack(side=TOP, fill=X, padx=2, pady=10)
    lab = Label(row, text="Search Single:", anchor='w')
    lab.pack(side=LEFT, padx=5, pady=5)

    sinStr = StringVar()  # textvariable is needed to allow callback for changes
    # sinStr.trace("w", lambda name, index, sv=sinStr: updateFields())
    sin = SearchBox(root, row, dropXFactor=1)
    sin._dataSet = ["zzap", "aayt", "atyu", "bogus", "bra", "bralet", "branford", "bridezilla", "bridge",
                   "burlap", "zill", "012345678901234567890123456789"]
    # name._dataSet = [("abc","middle","last"), ("abcd","ok","third"), ("abokquitebad","wrong","way")]
    sin.pack()

    row = Frame()
    row.pack(side=TOP, fill=X, padx=2, pady=10)
    lab = Label(row, text="Search Tuple:", anchor='w')
    lab.pack(side=LEFT, padx=5, pady=5)
    tupStr = StringVar()  # textvariable is needed to allow callback for changes
    # tupStr.trace("w", lambda name, index, sv=tupStr: updateFields())
    tup = SearchBox(root, row, dropXFactor=3)
    tup._dataSet = [("abc","ozland","last"), ("abcd","ok","third"), ("abokquitebad","oach tea","way")]
    tup.dIndex = 1
    tup.dataPercents = [25,25,50]
    tup.pack()

    row = Frame()
    row.pack(side=TOP, fill=X, padx=2, pady=5)
    lab = Label(row, text="CB Text:", anchor='w')
    lab.pack(side=LEFT)
    selUpdate = Label(row, text="", anchor='w')
    selUpdate.pack(side=LEFT, padx=5)

    row = Frame()
    row.pack(side=TOP, fill=X, padx=2, pady=10)
    b = ttk.Button(row, text='Cycle', command=tup.cycle)
    b.pack(side=LEFT)
    b = ttk.Button(row, text='Quit', command=root.quit)
    b.pack(side=RIGHT)

    tup.bind_selection(gotit)
    root.update()

    root.wm_protocol("WM_DELETE_WINDOW", root.quit)
    root.mainloop()

    print sin.selection()
    print tup.selection()