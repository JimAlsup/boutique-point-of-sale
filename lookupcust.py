'''
Copyright (c) <2016> Jim Alsup, Meia Alsup, Judy Chen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

from Tkinter import *
import re
import tkMessageBox
from openpyxl import load_workbook
from multicolumnlistbox import *


def selectCmd(evt):
    global selectedListItem
    selectedListItem = mlb.getSelectionIndex()
    # print "Select index ", selectedListItem


def cancel():
    global cancelled
    cancelled = True
    print "cancelled"
    top.quit()


def killTopWindow():
    global cancelled
    cancelled = True
    # print "Killed top window"
    top.quit()
    # no need to destroy the window.  t.quit exits via the mainloop
    # and this has a destory


def doubleClick( selected ):
    global selectedListItem
    selectedListItem = mlb.getSelectionIndex()
    # print "Sel>>> ", selectedListItem
    top.quit()


def cellValue( ws, r, c ):
    if ws.cell(row=r, column=c).data_type == 's':
        return ws.cell(row=r, column=c).value
    else:
        return ""


def mouseButton(event):
    # print("Button Press at: (%s %s)" % (event.x, event.y))
    return




def lookupCustomer(et, master, last, first, email):
    global top
    global cancelled
    global mlb

    cancelled = False
    matches = []
    lastNames = []
    firstNames = []
    emailAddrs = []

    wb = load_workbook(filename = et.customerDatabaseFileName(), read_only=True)
    wb._archive.fp.close()  # work around bug in load_workbook.
                            # Archive file is not properly closed - close it
    ws = wb['Sheet1']

    last = last.strip()
    first = first.strip()
    email = email.strip()
    doLast = len(last) > 0
    doFirst = len(first) > 0
    doEmail = len(email) > 0


    # match all last names, first and emails and build a list
    index = 1
    doAll = not doLast and not doFirst and not doEmail
    for rowX in ws.rows:
        rXa = rowX[0].value
        if rXa != None and rXa.strip()[0] == '#':
            continue  # skip comment lines
        rXb = rowX[1].value
        rXc = rowX[2].value
        rXat = rowX[0].data_type == 's'
        rXbt = rowX[1].data_type == 's'
        rXct = rowX[2].data_type == 's'
        record = FALSE
        if doAll:
            matches.append(index)
            record = True
        elif doLast and rXat and bool(re.match(last, rXa, re.I)):
            matches.append(index)
            record = True
            #print "C0> ", index, rXa, "  ", rXb, "  ", rXc
        elif doFirst and rXbt and bool(re.match(first, rXb, re.I)):
            matches.append(index)
            record = True
            # print "C1> ", index, rXb, "  ", rXa, "  ", rXc
        elif doEmail and rXct and bool(re.match(email, rXc, re.I)):
            matches.append(index)
            record = True
            # print "C2> ", index, rXc, "  ", rXa, "  ", rXb
        if record:
            if rXat: lastNames.append(rXa)
            else: lastNames.append("")
            if rXbt: firstNames.append(rXb)
            else: firstNames.append("")
            if rXct: emailAddrs.append(rXc)
            else: emailAddrs.append("")
            #print len(matches)-1, index, rXa, rXb, rXc
        index += 1
    # to grap arbitraty cell ws.cell(row=index, column=1).value
    # wb.save()

    if len(matches) == 0:
        tkMessageBox.showinfo(
            "Sorry", "No matches to last name, first name or email address found", parent=master
        )
        return "", "", ""

    frame = Toplevel()
    top = frame
    top.resizable(0, 0)

    frame.wm_title(et.appName() + ": Lookup Customer")

    # Sale Items
    row=Frame(frame)
    row.pack(side=TOP, fill=X, padx=5, pady=5)
    lab = Label(row, text="Matches:", anchor='w')
    lab.pack(side=LEFT)

    row = Frame(frame)
    row.pack(side=TOP, fill=X)
    lookupHeader = ['First                 ',
                    'Last                  ',
                    'EMail Address              ']
    mlb = MultiColumnListbox(row, lookupHeader, 12)
    for i in range(len(matches)):
        # print ">> ", cellValue(ws, i, 1), cellValue(ws, i, 2), cellValue(ws, i, 3)
        # print "#", i, matches[i], lastNames[i], firstNames[i], emailAddrs[i]
        try:
            mlb.tree.insert("", END, i+1, values=(firstNames[i], lastNames[i], emailAddrs[i]))
        except:
            print "Duplicate name found"
            # ignore

    mlb.tree.bind('<<TreeviewSelect>>', selectCmd )
    mlb.tree.bind('<Double-Button-1>', doubleClick )
    mlb.sortBy(mlb.getColumn())

    # mlb.pack(side=LEFT, fill=X, padx=10, pady=0, expand=1)

    row = Frame(frame)
    row.pack(side=TOP, fill=X, padx=5, pady=2)
    b1 = Button(row, text=' Cancel ', command=cancel)
    b1.pack(side=LEFT, padx=10, pady=10)
    b2 = Button(row, text=' Select ', command=frame.quit)
    b2.pack(side=RIGHT, padx=10, pady=10)

    frame.protocol("WM_DELETE_WINDOW", killTopWindow)  # Handle the [x] event
    #frame.transient(master)
    frame.grab_set()
    #master.wait_window(master)
    mlb.focusSelectTop()
    frame.iconbitmap('myicon.ico')
    frame.mainloop()
    if mlb.getSelectionIndex() == -1:  # no selection at exit
        cancelled = True
    frame.destroy()
    if cancelled:
        return "", "", ""
    return lastNames[selectedListItem], firstNames[selectedListItem], emailAddrs[selectedListItem]
    # frame.destroy()


def kill():
    print "App killed"
    root.destroy()

if __name__ == "__main__":
    root = Tk()
    root.title("lookuptest")
    root.withdraw()
    lastname, firstname, emailaddr = lookupCustomer(root, "", "", "")
    root.protocol("WM_DELETE_WINDOW", kill)
    root.deiconify()
    #root.destroy()
    print ">> ", lastname, firstname, emailaddr

    # root.mainloop()
